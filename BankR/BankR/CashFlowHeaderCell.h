//
//  CashFlowHeaderCell.h
//  BankR
//
//  Created by Ahmed Sadiq on 01/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CashFlowHeaderCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;

@end
