//
//  AddContributionVC.m
//  BankR
//
//  Created by Ahmed Sadiq on 03/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "AddContributionVC.h"
#import "LDProgressView.h"
#import "GoalContribution.h"
#import "DBManager.h"
#import "Utils.h"

@interface AddContributionVC ()

@end

@implementation AddContributionVC
@synthesize gModel;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    LDProgressView *progressView = [[LDProgressView alloc] initWithFrame:CGRectMake(0, 0, 280, 22)];
    progressView.color = [UIColor lightGrayColor];
    progressView.progress = gModel.completed;
    progressView.animate = @YES;
    progressView.type = LDProgressGradient;
    progressView.background = [progressView.color colorWithAlphaComponent:0.8];
    
    [self.progressView addSubview:progressView];
    
    tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

#pragma mark - Text Field Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)calenderBtnPressed:(id)sender {
    _calenderView.hidden = false;
}

- (IBAction)calenderDonePressed:(id)sender {
    _calenderView.hidden = true;
    
    NSDate *pickedDate = _datePicker.date;
    
    NSDateFormatter* myFormatter = [[NSDateFormatter alloc] init];
    [myFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString* myDateStr = [myFormatter stringFromDate:pickedDate];
    
    _dateTxt.text = myDateStr;
}

- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)donePressd:(id)sender {
    if(_dateTxt.text.length > 0 & _contributionAmount.text.length > 0) {
        GoalContribution *gcModel = [[GoalContribution alloc] init];
        gcModel.goalID = [gModel.goalID intValue];
        gcModel.contributionDate = _dateTxt.text;
        gcModel.contributionAmount = _contributionAmount.text;
        
        [[DBManager getSharedInstance] saveGoalContribution:gcModel];
        
        [self.navigationController popViewControllerAnimated:true];
    }
    else {
        [self presentViewController:[Utils showCustomAlert:@"Validation Error" andMessage:@"Please fill all fields with valid data"] animated:YES completion:nil];
    }
    
}
@end
