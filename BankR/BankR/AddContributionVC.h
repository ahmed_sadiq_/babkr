//
//  AddContributionVC.h
//  BankR
//
//  Created by Ahmed Sadiq on 03/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoalModel.h"

@interface AddContributionVC : UIViewController<UITextFieldDelegate> {
    UIGestureRecognizer *tapper;
}
@property (strong, nonatomic) IBOutlet UITextField *contributionAmount;
@property (strong, nonatomic) IBOutlet UITextField *noteTxt;

@property (strong, nonatomic) IBOutlet UIView *progressView;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) IBOutlet UITextField *dateTxt;
@property (strong, nonatomic) IBOutlet UIView *calenderView;

@property (strong, nonatomic) GoalModel *gModel;

- (IBAction)calenderBtnPressed:(id)sender;

- (IBAction)calenderDonePressed:(id)sender;

- (IBAction)backPressed:(id)sender;
- (IBAction)donePressd:(id)sender;

@end
