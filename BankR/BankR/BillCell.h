//
//  BillCell.h
//  BankR
//
//  Created by Ahmed Sadiq on 02/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"


@interface BillCell : SWTableViewCell

@property (strong, nonatomic) IBOutlet UILabel *amount;
@property (strong, nonatomic) IBOutlet UILabel *desc;
@property (strong, nonatomic) IBOutlet UILabel *dueDate;
@end
