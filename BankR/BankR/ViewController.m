//
//  ViewController.m
//  BankR
//
//  Created by Ahmed Sadiq on 26/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "ViewController.h"
#import "BankViewCell.h"
#import "AddAccountVC.h"
#import "BankDetailsVC.h"
#import "AccountModel.h"
#import "DBManager.h"

@interface ViewController ()

@end

@implementation ViewController

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    grandTotal = 0;
    self.accountsArray = [[DBManager getSharedInstance] getAllAccounts];
    [_bankTbView reloadData];
    
//    NSURL *movieURL = [NSURL URLWithString:@"http://ajo.witsapplication.com/assets/convertedVideo/hd_video2_335268585.m3u8"];
//    
//    // create a player view controller
//    _avPlayer = [AVPlayer playerWithURL:movieURL];
//    
//    AVPlayerViewController *controller = [[AVPlayerViewController alloc] init];
//    
//    [self addChildViewController:controller];
//    [self.view addSubview:controller.view];
//    
//    controller.view.frame = CGRectMake(0,0,375,400);
//    controller.player = _avPlayer;
//    controller.showsPlaybackControls = YES;
//    _avPlayer.closedCaptionDisplayEnabled = NO;
//    [_avPlayer pause];
//    [_avPlayer play];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.accountsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"BankViewCell";
    AccountModel *aModel = (AccountModel*)[self.accountsArray objectAtIndex:indexPath.row];
    BankViewCell *cell = (BankViewCell *)[_bankTbView dequeueReusableCellWithIdentifier:nil];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"BankViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    float totalAmount = [self getActualAMount:aModel];
    float startingBal = [aModel.startingBalance floatValue];
    if(!aModel.isPositive){
        startingBal= startingBal*-1;
    }
    
    totalAmount = startingBal+totalAmount;
    
    cell.accountName.text = aModel.accountNumber;
    
    
    
    cell.amountLbl.text = [NSString stringWithFormat:@"$%.1f",totalAmount];
    
    grandTotal = grandTotal+totalAmount;
    self.totalAmount.text = [NSString stringWithFormat:@"$%.1f",grandTotal];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(float) getActualAMount :(AccountModel*)aModel {
    
    float totalAmount = 0.0;
    NSMutableArray *transcationsArray = [[DBManager getSharedInstance] getAllAccountEntries:aModel.accountID];
    
    for(int i=0; i<transcationsArray.count; i++) {
        AccountEntry *tModel = [transcationsArray objectAtIndex:i];
        float amount = [tModel.entryAmount floatValue];
        if(tModel.isPositive){
            totalAmount = totalAmount+amount;
        }
        else {
            totalAmount = totalAmount-amount;
        }
    }
    
    return totalAmount;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    AccountModel *aModelTemp = (AccountModel*)[self.accountsArray objectAtIndex:indexPath.row];
    
    BankDetailsVC *addAccountVC = [[BankDetailsVC alloc] initWithNibName:@"BankDetailsVC" bundle:nil];
    addAccountVC.aModel = aModelTemp;
    [self.navigationController pushViewController:addAccountVC animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
}

- (UITableViewCellEditingStyle) tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleDelete;
}

- (BOOL) tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void) tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    
    AccountModel *aModelTemp = [self.accountsArray objectAtIndex:sourceIndexPath.row];
    [self.accountsArray removeObjectAtIndex:sourceIndexPath.row];
    [self.accountsArray insertObject:aModelTemp atIndex:destinationIndexPath.row];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //remove the deleted object from your data source.
        //If your data source is an NSMutableArray, do this
        
        AccountModel *aModel = [self.accountsArray objectAtIndex:indexPath.row];
        [[DBManager getSharedInstance] deleteAccount:aModel.accountID];
        [[DBManager getSharedInstance] deleteAllAccountEntries:aModel.accountID];
        [self.accountsArray removeObject:aModel];
        //[self.accountsArray removeObjectAtIndex:indexPath.row];
        [tableView reloadData]; // tell table to refresh now
    }
    //else if(editingStyle == UITableViewCellEd)
}


- (IBAction)optionsBtnPressed:(id)sender {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    actionSheet.view.tintColor = [UIColor colorWithRed:0.549 green:0.549 blue:0.549 alpha:1.0];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped do nothing.
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Add Account" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self goToAddAccount];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Rearrange" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        _optionsBtn.hidden = TRUE;
        _doneBtn.hidden = FALSE;
        
        [_bankTbView setEditing:TRUE animated:YES];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Delete Account" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        _optionsBtn.hidden = TRUE;
        _doneBtn.hidden = FALSE;
        
        [_bankTbView setEditing:TRUE animated:YES];
        
    }]];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (actionSheet.tag == 100) {
        NSLog(@"The Normal action sheet.");
    }
    else if (actionSheet.tag == 200){
        NSLog(@"The Delete confirmation action sheet.");
    }
    else{
        NSLog(@"The Color selection action sheet.");
    }
    
    NSLog(@"Index = %d - Title = %@", buttonIndex, [actionSheet buttonTitleAtIndex:buttonIndex]);
}

-(void)goToAddAccount {
    AddAccountVC *addAccountVC = [[AddAccountVC alloc] initWithNibName:@"AddAccountVC" bundle:nil];
    [self.navigationController pushViewController:addAccountVC animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
}

- (IBAction)doneBtnPressed:(id)sender {
    
    [_bankTbView setEditing:false animated:YES];
    
    _optionsBtn.hidden = false;
    _doneBtn.hidden = true;
    
    [[DBManager getSharedInstance] deleteAllAccounts];
    [self saveNewOrderOfAccounts];

    
}
-(void) saveNewOrderOfAccounts {
    for (int i=0; i<self.accountsArray.count; i++) {
        AccountModel *aModelTemp = (AccountModel*)[self.accountsArray objectAtIndex:i];
        [[DBManager getSharedInstance] saveAccountModel:aModelTemp];
    }
}



@end
