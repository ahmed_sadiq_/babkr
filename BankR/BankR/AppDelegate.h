//
//  AppDelegate.h
//  BankR
//
//  Created by Ahmed Sadiq on 26/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "CashFlowVC.h"
#import "BillTrackerVC.h"
#import "GoalsVC.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property ( strong , nonatomic ) UINavigationController *navigationController;
@property ( strong , nonatomic ) ViewController *viewController;
@property ( strong , nonatomic ) UITabBarController *tabBarController;

@end

