//
//  BillDetailVC.h
//  BankR
//
//  Created by Ahmed Sadiq on 02/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BillModel.h"

@interface BillDetailVC : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) BillModel *bModel;
@property (strong, nonatomic) IBOutlet UILabel *billDescText;
@property (strong, nonatomic) IBOutlet UILabel *dueDateTxt;
@property (strong, nonatomic) IBOutlet UILabel *totalDueAmount;
@property (strong, nonatomic) IBOutlet UILabel *amountPaidTxt;
@property (strong, nonatomic) IBOutlet UILabel *remianingDueTxt;

@property (strong, nonatomic) NSMutableArray *paymentArray;
@property (strong, nonatomic) NSMutableArray *paymentArrayForMonth;

@property (strong, nonatomic) IBOutlet UITableView *billTblView;
- (IBAction)backPressed:(id)sender;
- (IBAction)addPaymentPressed:(id)sender;
- (IBAction)quickPayPressed:(id)sender;
@end
