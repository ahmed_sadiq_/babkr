//
//  AddAccountVC.m
//  BankR
//
//  Created by Ahmed Sadiq on 27/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "AddAccountVC.h"
#import "AccountModel.h"
#import "DBManager.h"
#import "Utils.h"
@interface AddAccountVC ()

@end

@implementation AddAccountVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    view1.layer.borderColor = [UIColor grayColor].CGColor;
    view1.layer.borderWidth = 1.0f;
    
    view2.layer.borderColor = [UIColor grayColor].CGColor;
    view2.layer.borderWidth = 1.0f;
    
    tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)donePressed:(id)sender {
    if(accountNumTxt.text.length < 1 || startingBalanceTxt.text.length < 1) {
        [self presentViewController:[Utils showCustomAlert:@"Validation Error" andMessage:@"Please fill all fields with valid data"] animated:YES completion:nil];
    }
    else {
        AccountModel *aModel = [[AccountModel alloc] init];
        aModel.accountName = @"";
        aModel.accountNumber = accountNumTxt.text;
        aModel.startingBalance = startingBalanceTxt.text;
        aModel.isPositive = !isNegative;
        BOOL transFlag = [[DBManager getSharedInstance] saveAccountModel:aModel];
        if(transFlag) {
            [self.navigationController popViewControllerAnimated:true];
        }
        else {
            [self presentViewController:[Utils showCustomAlert:@"Something went wrong" andMessage:@"Please retry after some time"] animated:YES completion:nil];
        }
    }
    
}

- (IBAction)segmetPressed:(id)sender {
    switch (segmetCont.selectedSegmentIndex)
    {
        case 0:
            isNegative = false;
            break;
        case 1:
            isNegative = true;
            break;
        default: 
            break; 
    }
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

#pragma mark - Text Field Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    //[self animateTextField:nil up:YES];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //[self animateTextField:nil up:NO];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
@end
