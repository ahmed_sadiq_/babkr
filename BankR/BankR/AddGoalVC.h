//
//  AddGoalVC.h
//  BankR
//
//  Created by Ahmed Sadiq on 03/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddGoalVC : UIViewController {
    UIGestureRecognizer *tapper;
}

@property (strong, nonatomic) IBOutlet UITextField *goalDescTxt;
@property (strong, nonatomic) IBOutlet UITextField *goalAmountTxt;
@property (strong, nonatomic) IBOutlet UIView *progressView;


- (IBAction)donePressed:(id)sender;
- (IBAction)backPressed:(id)sender;

@end
