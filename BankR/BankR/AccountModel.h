//
//  AccountModel.h
//  BankR
//
//  Created by Ahmed Sadiq on 14/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AccountModel : NSObject


@property int accountID;
@property (strong, nonatomic) NSString *accountNumber;
@property (strong, nonatomic) NSString *accountName;
@property (strong, nonatomic) NSString *startingBalance;
@property BOOL isPositive;
@property BOOL isTranscationCleared;

@end
