//
//  BillDetailVC.m
//  BankR
//
//  Created by Ahmed Sadiq on 02/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "BillDetailVC.h"
#import "BillSplitDownCell.h"
#import "PaymentModel.h"
#import "DBManager.h"
#import "Utils.h"

@interface BillDetailVC ()

@end

@implementation BillDetailVC
@synthesize bModel;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSDate *dueDate = [self getStringFromDate:bModel.dueDate];
    
    _billDescText.text = bModel.billDesc;
    _dueDateTxt.text = [self getDateFromString:dueDate];
    _totalDueAmount.text = bModel.amount;
    
    _paymentArray = [[DBManager getSharedInstance] getAllPayments:bModel.billID];
    [self getPaymentsForMonth];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _paymentArrayForMonth.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    BillSplitDownCell *cell = (BillSplitDownCell *)[_billTblView dequeueReusableCellWithIdentifier:nil];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"BillSplitDownCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    PaymentModel *pModel = [_paymentArrayForMonth objectAtIndex:indexPath.row];
    
    cell.amountTxt.text = pModel.amount;
    cell.paidDateTxt.text = [NSString stringWithFormat:@"Paid %@",pModel.paymentDate];
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 64;
}



- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)addPaymentPressed:(id)sender {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"New Payment"
                                  message:@"Enter Payment Details"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {
                                                   //Do Some action here
                                                   UITextField *desc = [alert.textFields objectAtIndex:0];
                                                   UITextField *amount = [alert.textFields objectAtIndex:1];
                                                   
                                                   if(desc.text.length >0 && amount.text.length >0) {
                                                       PaymentModel *pModel = [[PaymentModel alloc] init];
                                                       pModel.billID = bModel.billID;
                                                       pModel.amount = amount.text;
                                                       pModel.paymentDate = [self getDateFromString:[NSDate date]];
                                                       
                                                       [[DBManager getSharedInstance] savePayment:pModel];
                                                       [_paymentArray addObject:pModel];
                                                       [self getPaymentsForMonth];
                                                       
                                                       [_billTblView reloadData];
                                                   }
                                                   else {
                                                       [self presentViewController:[Utils showCustomAlert:@"Validation Error" andMessage:@"All fields should be filled with proper data"] animated:YES completion:nil];
                                                   }
                                                   
                                                   
                                               }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                   }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Description";
    }];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Amount";
        textField.secureTextEntry = YES;
    }];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void) getPaymentsForMonth {
    int paid = 0;
    _paymentArrayForMonth = [[NSMutableArray alloc] init];
    for(int i=0; i<_paymentArray.count; i++) {
        PaymentModel *pModel = (PaymentModel*)[_paymentArray objectAtIndex:i];
        NSDate *dateOfPayment  = [self getFormattedDateFromSring:pModel.paymentDate];
        
        NSDateComponents *components1 = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:dateOfPayment];
        
        NSDateComponents *components2 = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
        
        NSInteger year1 = [components1 year];
        NSInteger month1 = [components1 month];
        
        NSInteger year2 = [components2 year];
        NSInteger month2 = [components2 month];
        
        if(year1 == year2 && month1 == month2) {
            [_paymentArrayForMonth addObject:pModel];
            paid = paid+[pModel.amount intValue];
        }
        
    }
    _amountPaidTxt.text = [NSString stringWithFormat:@"%d",paid];
    _remianingDueTxt.text = [NSString stringWithFormat:@"%d",[_totalDueAmount.text intValue] - paid];
    
}

- (IBAction)quickPayPressed:(id)sender {
    
    NSString *combo = [NSString stringWithFormat:@"%@%@",bModel.billID,[self getDateFromString:[NSDate date]]];
    
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:combo];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.navigationController popViewControllerAnimated:true];
    
}

-(NSString *)getDateFromString:(NSDate *)pstrDate
{
    NSDateFormatter* myFormatter = [[NSDateFormatter alloc] init];
    [myFormatter setDateFormat:@"MMM dd, yyyy"];
    NSString* myDateStr = [myFormatter stringFromDate:pstrDate];
    return myDateStr;
}

- (NSDate *)getStringFromDate:(NSString *)pstrDate
{
    NSDateFormatter* myFormatter = [[NSDateFormatter alloc] init];
    [myFormatter setDateFormat:@"MM-dd-yyyy"];
    NSDate* myDate = [myFormatter dateFromString:pstrDate];
    return myDate;
}

- (NSDate *)getFormattedDateFromSring:(NSString *)pstrDate
{
    NSDateFormatter* myFormatter = [[NSDateFormatter alloc] init];
    [myFormatter setDateFormat:@"MMM dd, yyyy"];
    NSDate* myDate = [myFormatter dateFromString:pstrDate];
    return myDate;
}

@end
