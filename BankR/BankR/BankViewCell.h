//
//  BankViewCell.h
//  BankR
//
//  Created by Ahmed Sadiq on 27/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BankViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *amountLbl;
@property (strong, nonatomic) IBOutlet UILabel *accountName;

@end
