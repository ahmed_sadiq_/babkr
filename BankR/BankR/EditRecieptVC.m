//
//  EditRecieptVC.m
//  BankR
//
//  Created by Ahmed Sadiq on 31/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "EditRecieptVC.h"
#import "Utils.h"
#import "DBManager.h"

@interface EditRecieptVC ()

@end

@implementation EditRecieptVC
@synthesize isEdit,tModel;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    view1.layer.borderColor = [UIColor grayColor].CGColor;
    view1.layer.borderWidth = 1.0f;
    
    view2.layer.borderColor = [UIColor grayColor].CGColor;
    view2.layer.borderWidth = 1.0f;
    
    calculatorView.layer.borderColor = [UIColor grayColor].CGColor;
    calculatorView.layer.borderWidth = 1.0f;
    
    tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
    
    if(isEdit) {
        transactiontypeTxt.text = tModel.entryDesc;
        amountSpendTxt.text = tModel.entryAmount;
        dateTxt.text = tModel.entryDate;
        calculatorBtn.hidden = true;
    }
    else {
        calculatorBtn.hidden = false;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
    
}

#pragma mark - Text Field Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)backPressed:(id)sender {
    
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)donePressed:(id)sender {
    
    if(isEdit) {
        if(transactiontypeTxt.text.length < 1 || amountSpendTxt.text.length < 1 || dateTxt.text.length < 1) {
            [self presentViewController:[Utils showCustomAlert:@"Validation Error" andMessage:@"Please fill all fields with valid data"] animated:YES completion:nil];
        }
        else {
            tModel.entryDesc = transactiontypeTxt.text;
            tModel.entryAmount = amountSpendTxt.text;
            tModel.entryDate = dateTxt.text;
            tModel.isPositive = !isNegative;
            BOOL transFlag = [[DBManager getSharedInstance] editAccountEntryModel:tModel];
            if(transFlag) {
                [self.navigationController popViewControllerAnimated:true];
            }
            else {
                [self presentViewController:[Utils showCustomAlert:@"Something went wrong" andMessage:@"Please retry after some time"] animated:YES completion:nil];
            }
        }
    }
    else {
        if(transactiontypeTxt.text.length < 1 || amountSpendTxt.text.length < 1 || dateTxt.text.length < 1) {
            [self presentViewController:[Utils showCustomAlert:@"Validation Error" andMessage:@"Please fill all fields with valid data"] animated:YES completion:nil];
        }
        else {
            AccountEntry *aeModel = [[AccountEntry alloc] init];
            aeModel.accountID = self.accountID;
            aeModel.entryDesc = transactiontypeTxt.text;
            aeModel.entryAmount = amountSpendTxt.text;
            aeModel.entryDate = dateTxt.text;
            aeModel.isPositive = !isNegative;
            aeModel.isTranscationCleared = true;
            BOOL transFlag = [[DBManager getSharedInstance] saveAccountEntryModel:aeModel andAccountID:self.accountID];
            if(transFlag) {
                [self.navigationController popViewControllerAnimated:true];
            }
            else {
                [self presentViewController:[Utils showCustomAlert:@"Something went wrong" andMessage:@"Please retry after some time"] animated:YES completion:nil];
            }
        }
    }
}

- (IBAction)datePressed:(id)sender {
    dateView.hidden = false;
}

- (IBAction)dateDnePressed:(id)sender {
    dateView.hidden = true;
    
    NSDate *pickedDate = datePicker.date;
    
    NSDateFormatter* myFormatter = [[NSDateFormatter alloc] init];
    [myFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString* myDateStr = [myFormatter stringFromDate:pickedDate];
    
    dateTxt.text = myDateStr;
    
}

- (IBAction)calculatorPressed:(id)sender {
    calculatorView.hidden = false;
}

- (IBAction)calculatorDonePressed:(id)sender {
    calculatorView.hidden = true;
}


#pragma mark - Calculator

- (IBAction) clearDisplay {
    display.text = @"";
}

- (IBAction) button1 {
    display.text=[NSString stringWithFormat:@"%@1",display.text];
}
- (IBAction) button2 {
    display.text=[NSString stringWithFormat:@"%@2",display.text];
}
- (IBAction) button3 {
    display.text=[NSString stringWithFormat:@"%@3",display.text];
}

- (IBAction) button4 {
    display.text=[NSString stringWithFormat:@"%@4",display.text];
}

- (IBAction) button5 {
    display.text=[NSString stringWithFormat:@"%@5",display.text];
}

- (IBAction) button6 {
    display.text=[NSString stringWithFormat:@"%@6",display.text];
}

- (IBAction) button7 {
    display.text=[NSString stringWithFormat:@"%@7",display.text];
}

- (IBAction) button8 {
    display.text=[NSString stringWithFormat:@"%@8",display.text];
}

- (IBAction) button9 {
    display.text=[NSString stringWithFormat:@"%@9",display.text];
}

- (IBAction) button0 {
    display.text=[NSString stringWithFormat:@"%@0",display.text];
}

- (IBAction) plusbutton {
    operation = Plus;
    storage = display.text;
    display.text=@"";
}

- (IBAction) minusbutton {
    operation = Minus;
    storage = display.text;
    display.text=@"";
}

- (IBAction) multiplybutton {
    operation = Multiply;
    storage = display.text;
    display.text=@"";
}

- (IBAction) dividebutton {
    operation = Divide;
    storage = display.text;
    display.text=@"";
}

- (IBAction) equalsbutton {
    NSString *val = display.text;
    switch(operation) {
        case Plus :
            display.text= [NSString stringWithFormat:@"%qi",[val longLongValue]+[storage longLongValue]];
            break;
        case Minus:
            display.text= [NSString stringWithFormat:@"%qi",[storage longLongValue]-[val longLongValue]];
            break;
        case Divide:
            display.text= [NSString stringWithFormat:@"%qi",[storage longLongValue]/[val longLongValue]];
            break;
        case Multiply:
            display.text= [NSString stringWithFormat:@"%qi",[val longLongValue]*[storage longLongValue]];
            break;
    }
}

- (IBAction)segmetPressed:(id)sender {
    switch (segmetCont.selectedSegmentIndex)
    {
        case 0:
            isNegative = false;
            break;
        case 1:
            isNegative = true;
            break;
        default:
            break;
    }
}


@end
