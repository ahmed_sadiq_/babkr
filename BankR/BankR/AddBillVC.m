//
//  AddBillVC.m
//  BankR
//
//  Created by Ahmed Sadiq on 02/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "AddBillVC.h"
#import "DBManager.h"
#import "Utils.h"

@interface AddBillVC ()

@end

@implementation AddBillVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _view1.layer.borderColor = [UIColor grayColor].CGColor;
    _view1.layer.borderWidth = 1.0f;
    
    _view2.layer.borderColor = [UIColor grayColor].CGColor;
    _view2.layer.borderWidth = 1.0f;
    
    tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
    
    
    
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Text Field Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    //[self animateTextField:nil up:YES];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //[self animateTextField:nil up:NO];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)dateDonePressed:(id)sender {
    dateView.hidden = true;
    if(isSpecificDate) {
        isSpecificDate = false;
        
        NSDate *pickedDate = datePicker.date;
        
        NSDateFormatter* myFormatter = [[NSDateFormatter alloc] init];
        [myFormatter setDateFormat:@"MM/dd/yyyy"];
        NSString* myDateStr = [myFormatter stringFromDate:pickedDate];
        
        _repeatBtn.titleLabel.text = myDateStr;
        
    }
    else {
        isDateSelected = true;
        
        NSDate *pickedDate = datePicker.date;
        
        NSDateFormatter* myFormatter = [[NSDateFormatter alloc] init];
        [myFormatter setDateFormat:@"MM/dd/yyyy"];
        NSString* myDateStr = [myFormatter stringFromDate:pickedDate];
        
        dueDateLbl.text = myDateStr;
    }
}

- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)donePressed:(id)sender {
    
    if(_addBillDesc.text.length > 0 && _amountTxt.text.length > 0 && isDateSelected) {
        BillModel *bModel = [[BillModel alloc] init];
        bModel.billDesc = _addBillDesc.text;
        bModel.amount = _amountTxt.text;
        bModel.dueDate = dueDateLbl.text;
        bModel.isRepeat = isRepeat;
        bModel.repeatUntil = _repeatBtn.titleLabel.text;
        
        [[DBManager getSharedInstance] saveBill:bModel];
        
        [self.navigationController popViewControllerAnimated:true];
    }
    else {
        [self presentViewController:[Utils showCustomAlert:@"Validation Error" andMessage:@"Please fill all fields with valid data"] animated:YES completion:nil];
    }
    
}

- (IBAction)repeatUntilPressed:(id)sender {
    arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:@" Forever",@" Specific Date",nil];
    NSArray * arrImage = nil;
    if(dropDown == nil) {
        CGFloat f = 180;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

- (IBAction)datePressed:(id)sender {
    dateView.hidden = false;
}

#pragma mark - Drop Down methods

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    
    [self rel];
    
    if(sender.selectedIndex == 0) {
        NSLog(@"Forever");
    }
    else {
        isSpecificDate = true;
        
        dateView.hidden = false;
        
    }
    
}

-(void)rel{
    //    [dropDown release];
    dropDown = nil;
}
- (IBAction)repeatPressed:(id)sender {
    if(isRepeat) {
        isRepeat = false;
    }
    else {
        isRepeat = true;
    }
}
@end
