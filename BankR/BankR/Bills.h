//
//  Bills.h
//  BankR
//
//  Created by Ahmed Sadiq on 14/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Bills : NSObject
@property int billID;
@property BOOL isRepeat;
@property (strong, nonatomic) NSString *billDesc;
@property (strong, nonatomic) NSString *billAmount;
@property (strong, nonatomic) NSString *dueDate;
@property (strong, nonatomic) NSString *repeatUntil;

@end
