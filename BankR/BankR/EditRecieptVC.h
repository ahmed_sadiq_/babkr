//
//  EditRecieptVC.h
//  BankR
//
//  Created by Ahmed Sadiq on 31/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "AccountEntry.h"


typedef enum{ Plus,Minus,Multiply,Divide} CalcOperation;

@interface EditRecieptVC : UIViewController<UITextFieldDelegate> {
    UIGestureRecognizer *tapper;
    
    
    IBOutlet UIView *view1;
    IBOutlet UIView *view2;
    
    
    BOOL isNegative;
    IBOutlet UISegmentedControl *segmetCont;
    
    IBOutlet UITextField *transactiontypeTxt;
    IBOutlet UITextField *amountSpendTxt;
    IBOutlet UITextField *dateTxt;
    IBOutlet UIView *dateView;
    IBOutlet UIDatePicker *datePicker;
    
    IBOutlet UITextField *display;
    IBOutlet UIButton *cbutton;
    NSString *storage;
    CalcOperation operation;
    IBOutlet UIView *calculatorView;
    IBOutlet UIButton *calculatorBtn;
    
    
    
}
@property BOOL isEdit;
@property int accountID;
@property (strong, nonatomic) AccountEntry *tModel;

- (IBAction)backPressed:(id)sender;
- (IBAction)donePressed:(id)sender;


- (IBAction)datePressed:(id)sender;
- (IBAction)dateDnePressed:(id)sender;

- (IBAction)calculatorPressed:(id)sender;
- (IBAction)calculatorDonePressed:(id)sender;

- (IBAction)segmetPressed:(id)sender;


- (IBAction) button1;
- (IBAction) button2;
- (IBAction) button3;
- (IBAction) button4;
- (IBAction) button5;
- (IBAction) button6;
- (IBAction) button7;
- (IBAction) button9;
- (IBAction) button0;
- (IBAction) plusbutton;
- (IBAction) equalsbutton;
- (IBAction) clearDisplay;

@end
