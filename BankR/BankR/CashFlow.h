//
//  CashFlow.h
//  BankR
//
//  Created by Ahmed Sadiq on 14/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CashFlow : NSObject
@property int cashFlowID;
@property (strong, nonatomic) NSString *groupName;
@property (strong, nonatomic) NSString *totalBalance;
@end
