//
//  AddGoalVC.m
//  BankR
//
//  Created by Ahmed Sadiq on 03/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "AddGoalVC.h"
#import "LDProgressView.h"
#import "Utils.h";
#import "DBManager.h";
#import "GoalModel.h"

@interface AddGoalVC ()

@end

@implementation AddGoalVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    LDProgressView *progressView = [[LDProgressView alloc] initWithFrame:CGRectMake(0, 0, 280, 22)];
    progressView.color = [UIColor lightGrayColor];
    progressView.progress = 0.00;
    progressView.animate = @YES;
    progressView.type = LDProgressGradient;
    progressView.background = [progressView.color colorWithAlphaComponent:0.8];
    
    [self.progressView addSubview:progressView];
    
    tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
}
- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)donePressed:(id)sender {
    
    if(_goalDescTxt.text.length > 0 && _goalAmountTxt.text.length > 0) {
        GoalModel *gModel = [[GoalModel alloc] init];
        gModel.goalName = _goalDescTxt.text;
        gModel.targetAmount = _goalAmountTxt.text;
        
        if([[DBManager getSharedInstance] saveGoal:gModel]) {
            [self.navigationController popViewControllerAnimated:true];
        }
        else {
            [self presentViewController:[Utils showCustomAlert:@"Database Error" andMessage:@"Unable to add this goal, please try again."] animated:YES completion:nil];
        }
        
        
    }
    else {
        [self presentViewController:[Utils showCustomAlert:@"Validation Error" andMessage:@"Please fill all fields with valid data"] animated:YES completion:nil];
    }
    
}

- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

#pragma mark - Text Field Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
@end
