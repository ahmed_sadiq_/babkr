//
//  GoalContribution.h
//  BankR
//
//  Created by Ahmed Sadiq on 14/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GoalContribution : NSObject

@property int contributionID;
@property int goalID;
@property (strong, nonatomic) NSString *contributionAmount;
@property (strong, nonatomic) NSString *contributionDate;
@end
