//
//  BankDetailsVC.h
//  BankR
//
//  Created by Ahmed Sadiq on 27/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PNChartDelegate.h"
#import "PNChart.h"
#import "AccountModel.h"


@interface BankDetailsVC : UIViewController<PNChartDelegate> {
    NSMutableArray *transcationsArray;
    NSMutableArray *uniqueDates;
    NSMutableDictionary *transactionDict;
    IBOutlet UIView *view1;
    IBOutlet UISegmentedControl *mainSegment;
    
    BOOL isRearrange;
    BOOL isLast30Days;
    
    IBOutlet UILabel *bankTitleLbl;
    
    IBOutlet UIView *chartView;
    
    
    IBOutlet UILabel *totalExpenseLbl;
    IBOutlet UILabel *averageSpendLbl;
    IBOutlet UILabel *totalTransactions;
    
    IBOutlet UILabel *daysLbl;
}
@property (strong, nonatomic) IBOutlet UILabel *totalSubAmountLbl;
@property (nonatomic) PNLineChart * lineChart;
@property (strong, nonatomic) IBOutlet UITableView *recieptTblView;
@property (strong, nonatomic) AccountModel *aModel;

- (IBAction)optionsPressed:(id)sender;

- (IBAction)segmetChanged:(id)sender;


@end
