//
//  CashFlowGroupDetailsVC.m
//  BankR
//
//  Created by Ahmed Sadiq on 31/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "CashFlowGroupDetailsVC.h"
#import "RecieptDetailCell.h"
#import "CashFlowGroupDetailsVC.h"
#import "CashFlowHeaderCell.h"
#import "DBManager.h"
#import "Utils.h"
#import <QuartzCore/QuartzCore.h>

@interface CashFlowGroupDetailsVC ()

@end

@implementation CashFlowGroupDetailsVC
@synthesize cModel;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    subTotal = 0;
    newExpense.layer.borderColor = [UIColor grayColor].CGColor;
    newExpense.layer.borderWidth = 1.0f;
    newExpense.layer.cornerRadius = 8;
    newExpense.layer.masksToBounds = YES;
    
    calculatorView.layer.borderColor = [UIColor grayColor].CGColor;
    calculatorView.layer.borderWidth = 1.0f;
    
    transcationsArray = [[DBManager getSharedInstance] getAllCashflowExpense:cModel.cashFlowID];
    
    
    tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
    
    [self totalAmountText];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return transcationsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CashFlowExpense *tModel = [transcationsArray objectAtIndex:indexPath.row];
    
    if(tModel.isHeader) {
        
        static NSString *simpleTableIdentifier = @"CashFlowHeaderCell";
        
        CashFlowHeaderCell *cell = (CashFlowHeaderCell *)[mainTblView dequeueReusableCellWithIdentifier:nil];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CashFlowHeaderCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.titleLbl.text = tModel.headerTitle;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    else {
        static NSString *simpleTableIdentifier = @"RecieptDetailCell";
        
        RecieptDetailCell *cell = (RecieptDetailCell *)[mainTblView dequeueReusableCellWithIdentifier:nil];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"RecieptDetailCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.transType.text = tModel.expenseDesc;
        cell.amount.text = tModel.expenseAmount;
        
        cell.checkBtn.tag = indexPath.row;
        
        [cell.checkBtn addTarget:self
                          action:@selector(checkBtnPressed:)
                forControlEvents:UIControlEventTouchUpInside];
        
        
        if(!tModel.isPositive) {
            if(tModel.isCleared) {
                subTotal = subTotal-[tModel.expenseAmount intValue];
                
            }
            cell.amount.text = [NSString stringWithFormat:@"- %@",tModel.expenseAmount];
            
            cell.amount.textColor = [UIColor colorWithRed:0.823 green:0.243 blue:0.247 alpha:1.0];
            cell.transType.textColor = [UIColor colorWithRed:0.823 green:0.243 blue:0.247 alpha:1.0];
            
        }
        else {
            if(tModel.isCleared) {
                subTotal = subTotal+[tModel.expenseAmount intValue];
                
            }
            
            cell.amount.textColor = [UIColor colorWithRed:0.211 green:0.662 blue:0.266 alpha:1.0];
            cell.transType.textColor = [UIColor colorWithRed:0.211 green:0.662 blue:0.266 alpha:1.0];
        }
        
        cell.subTotal.text = [NSString stringWithFormat:@"$%d",subTotal];
        
        if(isRearrange) {
            [cell.checkBtn setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
            cell.arrowLbl.hidden = false;
        }
        else {
            if(tModel.isCleared) {
                [cell.checkBtn setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
            }
            else {
                [cell.checkBtn setImage:[UIImage imageNamed:@"checkbox_unfilled"] forState:UIControlStateNormal];
            }
            
            cell.arrowLbl.hidden = true;
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CashFlowExpense *tModel = [transcationsArray objectAtIndex:indexPath.row];
    if(tModel.isHeader) {
        return 28;
    }
    else {
        return 72;
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (UITableViewCellEditingStyle) tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleDelete;
}

- (BOOL) tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void) tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    
    NSInteger sourceRow = sourceIndexPath.row;
    NSInteger destRow = destinationIndexPath.row;
    
    id object = [transcationsArray objectAtIndex:sourceRow];
    
    [transcationsArray removeObjectAtIndex:sourceRow];
    [transcationsArray insertObject:object atIndex:destRow];
    
    [[DBManager getSharedInstance] deleteAllCashflowExpense:cModel.cashFlowID];
    
    for(int i=0; i<transcationsArray.count; i++) {
        CashFlowExpense *cfModel = [transcationsArray objectAtIndex:i];
        [[DBManager getSharedInstance] saveCashFlowExpense:cfModel];
    }
    
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove the row from data model
    [transcationsArray removeObjectAtIndex:indexPath.row];
    
    // Request table view to reload
    subTotal = 0;
    [tableView reloadData];
}

- (IBAction)optionsPressed:(id)sender {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    actionSheet.view.tintColor = [UIColor colorWithRed:0.549 green:0.549 blue:0.549 alpha:1.0];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped do nothing.
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Add New Entry" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        descTxt.text = @"";
        amountTxt.text = @"";
        newExpense.hidden = false;
        
        
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Rearrange" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        if(mainTblView.editing) {
            ttlLbl.hidden = false;
            doneBtn.hidden = true;
            optionBtn.hidden = false;
            [mainTblView setEditing:false animated:YES];
        }
        else {
            ttlLbl.hidden = true;
            doneBtn.hidden = false;
            optionBtn.hidden = true;
            [mainTblView setEditing:true animated:YES];
        }
        
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Add Header" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Add Header"
                                                                       message:@"Enter a new header title"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            // optionally configure the text field
            textField.keyboardType = UIKeyboardTypeAlphabet;
        }];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Save"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action) {
                                                             UITextField *textField = [alert.textFields firstObject];
                                                             
                                                             CashFlowExpense *tModel = [[CashFlowExpense alloc] init];
                                                             
                                                             tModel.isHeader = true;
                                                             tModel.cashflowID = cModel.cashFlowID;
                                                             tModel.headerTitle = textField.text;
                                                             [transcationsArray addObject:tModel];
                                                             
                                                             [[DBManager getSharedInstance] saveCashFlowExpense:tModel];
                                                             transcationsArray = [[DBManager getSharedInstance] getAllCashflowExpense:cModel.cashFlowID];
                                                             subTotal = 0;
                                                             [mainTblView reloadData];
                                                             
                                                         }];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                               style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction *action) {
                                                                 UITextField *textField = [alert.textFields firstObject];
                                                             }];
        
        [alert addAction:cancelAction];
        [alert addAction:okAction];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }]];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (IBAction)checkBtnPressed:(id)sender {
    
    UIButton *btnSender = (UIButton*) sender;
    
    CashFlowExpense *tModel = [transcationsArray objectAtIndex:(int)btnSender.tag];
    
    if(isRearrange) {
        //[self goRecieptAccount:tModel];
    }
    else {
        if(tModel.isCleared) {
            tModel.isCleared = false;
            
        }
        else {
            tModel.isCleared = true;
            
            
        }
        subTotal = 0;
        
        //NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:btnSender.tag inSection:0];
        //NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, nil];
        //[mainTblView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
        
        [[DBManager getSharedInstance] editCashFlowExpenseModel:tModel];
        
        [mainTblView reloadData];
        
    }
}

- (IBAction)doneBtnPressed:(id)sender {
    if(mainTblView.editing) {
        ttlLbl.hidden = false;
        optionBtn.hidden = false;
        doneBtn.hidden = true;
        [mainTblView setEditing:false animated:YES];
    }
}

- (IBAction)calculatorPressed:(id)sender {
    calculatorView.hidden = false;
}

- (IBAction)expenseCancelPressed:(id)sender {
    newExpense.hidden = true;
}

- (IBAction)expenseSavePressed:(id)sender {
    
    if(descTxt.text.length < 1 || amountTxt.text.length < 1) {
        [self presentViewController:[Utils showCustomAlert:@"Validation Error" andMessage:@"All fields are required and should be in proper format"] animated:YES completion:nil];
    }
    else {
        
        CashFlowExpense *cfModel = [[CashFlowExpense alloc] init];
        
        cfModel.cashflowID = cModel.cashFlowID;
        cfModel.isHeader = false;
        cfModel.headerID = 0;
        cfModel.headerTitle = @"";
        cfModel.isPositive = !isNegative;
        cfModel.isCleared = true;
        cfModel.expenseAmount = amountTxt.text;
        cfModel.expenseDesc = descTxt.text;
        
        [[DBManager getSharedInstance] saveCashFlowExpense:cfModel];
        transcationsArray = [[DBManager getSharedInstance] getAllCashflowExpense:cModel.cashFlowID];
        subTotal = 0;
        [mainTblView reloadData];
        [self totalAmountText];
        
        newExpense.hidden = true;
    }
    
}

- (IBAction)valueChangedPressed:(id)sender {
    if(isNegative) {
        isNegative = false;
        
    }
    else {
        isNegative = true;
    }
}

#pragma mark - Text Field Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    //[self animateTextField:nil up:YES];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //[self animateTextField:nil up:NO];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

#pragma mark - Calculator

- (IBAction) clearDisplay {
    display.text = @"";
}

- (IBAction) button1 {
    display.text=[NSString stringWithFormat:@"%@1",display.text];
}
- (IBAction) button2 {
    display.text=[NSString stringWithFormat:@"%@2",display.text];
}
- (IBAction) button3 {
    display.text=[NSString stringWithFormat:@"%@3",display.text];
}

- (IBAction) button4 {
    display.text=[NSString stringWithFormat:@"%@4",display.text];
}

- (IBAction) button5 {
    display.text=[NSString stringWithFormat:@"%@5",display.text];
}

- (IBAction) button6 {
    display.text=[NSString stringWithFormat:@"%@6",display.text];
}

- (IBAction) button7 {
    display.text=[NSString stringWithFormat:@"%@7",display.text];
}

- (IBAction) button8 {
    display.text=[NSString stringWithFormat:@"%@8",display.text];
}

- (IBAction) button9 {
    display.text=[NSString stringWithFormat:@"%@9",display.text];
}

- (IBAction) button0 {
    display.text=[NSString stringWithFormat:@"%@0",display.text];
}

- (IBAction) plusbutton {
    operation = Plus;
    storage = display.text;
    display.text=@"";
}

- (IBAction) minusbutton {
    operation = Minus;
    storage = display.text;
    display.text=@"";
}

- (IBAction) multiplybutton {
    operation = Multiply;
    storage = display.text;
    display.text=@"";
}

- (IBAction) dividebutton {
    operation = Divide;
    storage = display.text;
    display.text=@"";
}

- (IBAction) equalsbutton {
    NSString *val = display.text;
    switch(operation) {
        case Plus :
            display.text= [NSString stringWithFormat:@"%qi",[val longLongValue]+[storage longLongValue]];
            break;
        case Minus:
            display.text= [NSString stringWithFormat:@"%qi",[storage longLongValue]-[val longLongValue]];
            break;
        case Divide:
            display.text= [NSString stringWithFormat:@"%qi",[storage longLongValue]/[val longLongValue]];
            break;
        case Multiply:
            display.text= [NSString stringWithFormat:@"%qi",[val longLongValue]*[storage longLongValue]];
            break;
    }
}
- (IBAction)donePressed:(id)sender {
    calculatorView.hidden = true;
}

- (void) totalAmountText {
    int totalAmountInt = 0;
    
    for(int i=0; i<transcationsArray.count; i++) {
        CashFlowExpense *cfModel = [transcationsArray objectAtIndex:i];
        
        if(cfModel.isPositive) {
            totalAmountInt = totalAmountInt + [cfModel.expenseAmount intValue];
        }
        else {
            totalAmountInt = totalAmountInt - [cfModel.expenseAmount intValue];
        }
        
    }
    totalAmount.text = [NSString stringWithFormat:@"$%d",totalAmountInt];
}
@end
