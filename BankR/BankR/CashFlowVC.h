//
//  CashFlowVC.h
//  BankR
//
//  Created by Ahmed Sadiq on 26/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CashFlowVC : UIViewController <UITableViewDelegate,UITableViewDataSource> {
    NSMutableArray *cashflowArray;
    BOOL isDuplicate;
}

@property (strong, nonatomic) IBOutlet UIButton *doneBtn;
@property (strong, nonatomic) IBOutlet UIButton *optionsBtn;

@property (strong, nonatomic) IBOutlet UITableView *cashflowTblView;
- (IBAction)optionsPressed:(id)sender;
- (IBAction)donePressed:(id)sender;
@end
