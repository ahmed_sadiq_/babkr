//
//  AppDelegate.m
//  BankR
//
//  Created by Ahmed Sadiq on 26/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "AppDelegate.h"


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [self createTabBarAndControl];
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)createTabBarAndControl {
    
    self.viewController = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
    
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:self.viewController];
    [self.navigationController.tabBarItem setSelectedImage:[[UIImage imageNamed:@"home"]
                                                            imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal]];
    
    [self.navigationController.tabBarItem setImage:[[UIImage imageNamed:@"home_unsel.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    self.navigationController.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    UIViewController *cashFlow;
    cashFlow = [[CashFlowVC alloc] initWithNibName:@"CashFlowVC" bundle:[NSBundle mainBundle]];
    
    
    UINavigationController *cashFlowNavController = [[UINavigationController alloc] initWithRootViewController:cashFlow];
    
    
    [cashFlow.tabBarItem setSelectedImage:[[UIImage imageNamed:@"Cashflow_unsel"]
                                            imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal]];
    [cashFlow.tabBarItem setImage:[[UIImage imageNamed:@"Cashflow"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    
    cashFlow.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    
    [cashFlow.navigationController setNavigationBarHidden:YES animated:NO];
    
    UIViewController *billTracker;
    billTracker = [[BillTrackerVC alloc] initWithNibName:@"BillTrackerVC" bundle:[NSBundle mainBundle]];
    UINavigationController *billTrackerNavController = [[UINavigationController alloc] initWithRootViewController:billTracker];
    
    [billTracker.tabBarItem setSelectedImage:[[UIImage imageNamed:@"leaf_unsel"]
                                          imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal]];
    [billTracker.tabBarItem setImage:[[UIImage imageNamed:@"leaf"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    billTracker.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    
    [billTracker.navigationController setNavigationBarHidden:YES animated:NO];
    
    UIViewController *goals;
    goals = [[GoalsVC alloc] initWithNibName:@"GoalsVC" bundle:[NSBundle mainBundle]];
    UINavigationController *goalNavController = [[UINavigationController alloc] initWithRootViewController:goals];
    
    [goals.tabBarItem setSelectedImage:[[UIImage imageNamed:@"checked_sel"]
                                            imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal]];
    [goals.tabBarItem setImage:[[UIImage imageNamed:@"checked"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    goals.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    
    [goals.navigationController setNavigationBarHidden:YES animated:NO];
    
    self.tabBarController = [[UITabBarController alloc] init] ;
    self.tabBarController.viewControllers = [NSArray arrayWithObjects:self.navigationController, cashFlowNavController,billTrackerNavController,goalNavController,nil];
    
    [self.tabBarController.tabBar setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:0.964 green:0.964 blue:0.964 alpha:1.0]]];
    
    self.viewController.navigationController.navigationBar.tintColor = [UIColor blackColor];
    //self.viewController.navigationController.navigationBar
    [[[UIApplication sharedApplication]delegate] window].rootViewController = self.tabBarController;
}

- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}



@end
