//
//  ExpenseHeader.h
//  BankR
//
//  Created by Ahmed Sadiq on 14/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExpenseHeader : NSObject
@property int headerID;
@property (strong, nonatomic) NSString *headerName;
@end
