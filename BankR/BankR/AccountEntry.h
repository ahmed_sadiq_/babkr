//
//  AccountEntry.h
//  BankR
//
//  Created by Ahmed Sadiq on 14/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AccountEntry : NSObject
@property int accountEntryID;
@property int accountID;
@property (strong, nonatomic) NSString *entryDesc;
@property (strong, nonatomic) NSString *entryAmount;
@property (strong, nonatomic) NSString *entryDate;
@property BOOL isPositive;
@property BOOL isTranscationCleared;
@end
