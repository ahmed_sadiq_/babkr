//
//  GoalModel.h
//  BankR
//
//  Created by Ahmed Sadiq on 23/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GoalModel : NSObject

@property (strong, nonatomic) NSString *goalID;
@property (strong, nonatomic) NSString *goalName;
@property (strong, nonatomic) NSString *targetAmount;
@property double completed;
@end
