//
//  BillModel.h
//  BankR
//
//  Created by Ahmed Sadiq on 02/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BillModel : NSObject

@property (strong, nonatomic) NSString *billID;
@property (strong, nonatomic) NSString *dueDate;
@property (strong, nonatomic) NSString *amount;
@property (strong, nonatomic) NSString *billDesc;
@property (strong, nonatomic) NSString *repeatUntil;
@property BOOL isRepeat;


@end
