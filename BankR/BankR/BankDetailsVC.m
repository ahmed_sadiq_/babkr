//
//  BankDetailsVC.m
//  BankR
//
//  Created by Ahmed Sadiq on 27/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "BankDetailsVC.h"
#import "AccountEntry.h"
#import "RecieptDetailCell.h"
#import "EditRecieptVC.h"
#import "DBManager.h"
#import <QuartzCore/QuartzCore.h>

@interface BankDetailsVC ()

@end

@implementation BankDetailsVC
@synthesize aModel;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    view1.layer.borderColor = [UIColor grayColor].CGColor;
    view1.layer.borderWidth = 1.0f;
    
    bankTitleLbl.text = aModel.accountNumber;
    
    UIPinchGestureRecognizer *twoFingerPinch = [[UIPinchGestureRecognizer alloc]
                                                 initWithTarget:self
                                                 action:@selector(twoFingerPinch:)];
    
    [chartView addGestureRecognizer:twoFingerPinch];
}

- (void)twoFingerPinch:(UIPinchGestureRecognizer *)recognizer
{
    
    isLast30Days = true;
    
    transcationsArray = [[DBManager getSharedInstance] getAllAccountEntries:aModel.accountID];
    
    daysLbl.text = @"Last 30 Days";
    
    NSArray *viewsToRemove = [chartView subviews];
    for (UIView *v in viewsToRemove) {
        if(v.tag == 0)
            [v removeFromSuperview];
    }
    
    [self getUniqueDatesFromTransactions];
    [self prepareTransactionDictionary];
    [_recieptTblView reloadData];
    
    [self populateLineChart];
    
    
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    transcationsArray = [[DBManager getSharedInstance] getAllAccountEntries:aModel.accountID];
    
    
    [self getUniqueDatesFromTransactions];
    [self prepareTransactionDictionary];
    [_recieptTblView reloadData];
    
    
}

- (BOOL) checkIfTransactionIsInLast7Days : (NSDate*) transactionDate {
    NSDate *currentDate = [NSDate date];
    NSDate *dateSevenDaysPrior = [currentDate dateByAddingTimeInterval:-(7 * 24 * 60 * 60)];
    NSDate *someDate = transactionDate;
    
    if (([dateSevenDaysPrior compare:someDate] != NSOrderedDescending) &&
        ([someDate compare:currentDate] != NSOrderedDescending))
    {
        return true;
    }
    else
    {
        return false;
    }
    
    
}

- (BOOL) checkIfTransactionIsInLast30Days : (NSDate*) transactionDate {
    NSDate *currentDate = [NSDate date];
    NSDate *dateSevenDaysPrior = [currentDate dateByAddingTimeInterval:-(30 * 24 * 60 * 60)];
    NSDate *someDate = transactionDate;
    
    if (([dateSevenDaysPrior compare:someDate] != NSOrderedDescending) &&
        ([someDate compare:currentDate] != NSOrderedDescending))
    {
        return true;
    }
    else
    {
        return false;
    }
    
    
}


- (void) prepareTransactionDictionary {
    transactionDict = [[NSMutableDictionary alloc] init];
    
    for(int i=0; i<uniqueDates.count; i++) {
        NSMutableArray *arrayForDate = (NSMutableArray*)[self getTransactionArrayForDate:(NSDate*)[uniqueDates objectAtIndex:i]];
        NSString *keyDate = [self getDateFromString:(NSDate*)[uniqueDates objectAtIndex:i]];
        [transactionDict setValue:arrayForDate forKey:keyDate];
        
    }
}

- (NSMutableArray *) getTransactionArrayForDate : (NSDate *) transDate {
    NSMutableArray* transArray = [[NSMutableArray alloc] init];
    
    for(int i=0; i<transcationsArray.count; i++) {
        
        AccountEntry *tModel = [transcationsArray objectAtIndex:i];
        NSDate *transactionDate = [self getStringFromDate:tModel.entryDate];
        
        BOOL dateAvailable = [self dateComparision:transactionDate andDate2:transDate];
        
        if(dateAvailable) {
            [transArray addObject:tModel];
        }
    }
    
    return transArray;
}

- (void) getUniqueDatesFromTransactions {
    
    uniqueDates = [[NSMutableArray alloc] init];
    float totalAmount = 0.0;
    float totalExpense = 0.0;
    
    
    for(int i=0; i<transcationsArray.count; i++) {
        
        AccountEntry *tModel = [transcationsArray objectAtIndex:i];
        NSDate *transactionDate = [self getStringFromDate:tModel.entryDate];
        
        
        
        float amount = [tModel.entryAmount floatValue];
        
        if(tModel.isPositive){
            totalAmount = totalAmount+amount;
            
            if(!isLast30Days) {
                if([self checkIfTransactionIsInLast7Days:transactionDate]) {
                    totalExpense = totalExpense+amount;
                }
            }
            else {
                if([self checkIfTransactionIsInLast30Days:transactionDate]) {
                    totalExpense = totalExpense+amount;
                }
            }
        }
        else {
            totalAmount = totalAmount-amount;
        }
        
        if(![self checkIfDateExist:transactionDate]) {
            [uniqueDates addObject:transactionDate];
        }
    }
    
    float startingBal =[aModel.startingBalance floatValue];
    if(!aModel.isPositive){
        startingBal=startingBal*-1;
    }
    totalAmount = startingBal + totalAmount;
    
    NSArray *ascendingDates = [uniqueDates sortedArrayUsingSelector:@selector(compare:)];
    
    uniqueDates = [[[NSMutableArray arrayWithArray:ascendingDates] reverseObjectEnumerator] allObjects];
    
    //Used this loop to calculate the final count
    _totalSubAmountLbl.text = [NSString stringWithFormat:@"$%.1f",totalAmount];
    
    totalExpenseLbl.text = [NSString stringWithFormat:@"$%.1f",totalExpense];
    totalTransactions.text = [NSString stringWithFormat:@"%d",[self getNumberOfTranscation]];
    averageSpendLbl.text = [NSString stringWithFormat:@"$%.1f",(totalExpense/[self getNumberOfTranscation])];
}

- (int) getNumberOfTranscation {
    int numberOfTransactions = 0;
    
    for(int i=0; i<transcationsArray.count; i++) {
        
        AccountEntry *tModel = [transcationsArray objectAtIndex:i];
        NSDate *transactionDate = [self getStringFromDate:tModel.entryDate];
        
        if(!isLast30Days) {
            if([self checkIfTransactionIsInLast7Days:transactionDate]) {
                numberOfTransactions++;
            }
        }
        else {
            if([self checkIfTransactionIsInLast30Days:transactionDate]) {
                numberOfTransactions++;
            }
        }
    }
    return numberOfTransactions;
}

-(NSDate *)getStringFromDate:(NSString *)pstrDate
{
    NSDateFormatter* myFormatter = [[NSDateFormatter alloc] init];
    [myFormatter setDateFormat:@"MM-dd-yyyy"];
    NSDate* myDate = [myFormatter dateFromString:pstrDate];
    return myDate;
}

-(NSString *)getDateFromString:(NSDate *)pstrDate
{
    NSDateFormatter* myFormatter = [[NSDateFormatter alloc] init];
    [myFormatter setDateFormat:@"MM-dd-yyyy"];
    NSString* myDateStr = [myFormatter stringFromDate:pstrDate];
    return myDateStr;
}
-(NSString *)getShortDateString:(NSDate *)pstrDate
{
    NSDateFormatter* myFormatter = [[NSDateFormatter alloc] init];
    [myFormatter setDateFormat:@"MMM dd, yyyy"];
    NSString* myDateStr = [myFormatter stringFromDate:pstrDate];
    return myDateStr;
}

-(BOOL) checkIfDateExist : (NSDate*) transactionDate {
    
    for(int i=0; i<uniqueDates.count; i++) {
        NSDate *tempDate = (NSDate*)[uniqueDates objectAtIndex:i];
        
        BOOL dateAvailable = [self dateComparision:transactionDate andDate2:tempDate];
        if(dateAvailable) {
            return true;
        }
    }
    return false;
    
}

-(NSMutableArray*) getSectionArray : (int) section {
    NSDate *transDate = (NSDate*) [uniqueDates objectAtIndex:section];
    NSString *dateStr = [self getDateFromString:transDate];
    NSMutableArray *sectionArray = (NSMutableArray*)[transactionDict objectForKey:dateStr];
    
    return sectionArray;
}

-(BOOL)dateComparision:(NSDate*)date1 andDate2:(NSDate*)date2{
    
    BOOL isSameDate;
    
    if ([date1 compare:date2] == NSOrderedDescending || [date1 compare:date2] == NSOrderedAscending) {
        isSameDate = NO;
    } else {
        isSameDate = YES;
        
    }
    
    return isSameDate;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return uniqueDates.count;    //count of section
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSDate *sectionDate = (NSDate*)[uniqueDates objectAtIndex:section];
    return [self getDateFromString:sectionDate];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSMutableArray *sectionArray = [self getSectionArray:(int)section];
    
    return sectionArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"RecieptDetailCell";
    
    RecieptDetailCell *cell = (RecieptDetailCell *)[_recieptTblView dequeueReusableCellWithIdentifier:nil];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"RecieptDetailCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    NSMutableArray *sectionArray = [self getSectionArray:(int)indexPath.section];
    
    AccountEntry *tModel = [sectionArray objectAtIndex:indexPath.row];
    
    cell.transType.text = tModel.entryDesc;
    if(!tModel.isPositive){
        cell.amount.textColor = [UIColor redColor];
    }
    cell.amount.text = [NSString stringWithFormat:@"$%@",tModel.entryAmount];
    
    cell.checkBtn.tag = indexPath.section;
    int hintToBe = (int)indexPath.row;
    cell.checkBtn.accessibilityHint = [NSString stringWithFormat:@"%d",hintToBe];
    [cell.checkBtn addTarget:self
                      action:@selector(checkBtnPressed:)
            forControlEvents:UIControlEventTouchUpInside];
    
    if(isRearrange) {
        [cell.checkBtn setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
        cell.arrowLbl.hidden = false;
    }
    else {
        if(tModel.isTranscationCleared) {
            [cell.checkBtn setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
        }
        else {
            [cell.checkBtn setImage:[UIImage imageNamed:@"checkbox_unfilled"] forState:UIControlStateNormal];
        }
        
        cell.arrowLbl.hidden = true;
    }
    cell.subTotal.hidden = true;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 72;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        
        
        NSMutableArray *sectionArray = [self getSectionArray:(int)indexPath.section];
        
        AccountEntry *accountEntryModel = [sectionArray objectAtIndex:indexPath.row];
        
        [[DBManager getSharedInstance] deleteAccountEntry:accountEntryModel.accountEntryID];
        [sectionArray removeObject:accountEntryModel];
        
        transcationsArray = [[DBManager getSharedInstance] getAllAccountEntries:aModel.accountID];
        
        [self getUniqueDatesFromTransactions];
        [self prepareTransactionDictionary];
        [tableView reloadData];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)checkBtnPressed:(id)sender {
    
    UIButton *btnSender = (UIButton*) sender;
    
    int objectIndex = [btnSender.accessibilityHint intValue];
    
    NSMutableArray *sectionArray = [self getSectionArray:(int)btnSender.tag];
    AccountEntry *tModel = [sectionArray objectAtIndex:objectIndex];
    
    if(isRearrange) {
        [self goRecieptAccount:tModel];
    }
    else {
        if(tModel.isTranscationCleared) {
            tModel.isTranscationCleared = false;
        }
        else {
            tModel.isTranscationCleared = true;
        }
        
        NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:[btnSender.accessibilityHint intValue] inSection:btnSender.tag];
        NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, nil];
        [self.recieptTblView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
        
    }
    [[DBManager getSharedInstance] editAccountEntryModel:tModel];
}
- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)optionsPressed:(id)sender {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    actionSheet.view.tintColor = [UIColor colorWithRed:0.549 green:0.549 blue:0.549 alpha:1.0];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped do nothing.
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Add New Entry" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self goRecieptAccount:nil];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Re-Arrange" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self rearrangeListView];
        
    }]];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (IBAction)segmetChanged:(id)sender {
    if(mainSegment.selectedSegmentIndex == 0) {
        chartView.hidden = true;
    }
    else {
        [self populateLineChart];
        chartView.hidden = false;
    }
}

-(void)goRecieptAccount : (AccountEntry*) tModel {
    EditRecieptVC *editRecieptVC = [[EditRecieptVC alloc] initWithNibName:@"EditRecieptVC" bundle:nil];
    if(tModel) {
        editRecieptVC.isEdit = true;
        editRecieptVC.tModel = tModel;
    }
    editRecieptVC.accountID = aModel.accountID;
    [self.navigationController pushViewController:editRecieptVC animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
}


- (void) rearrangeListView {
    isRearrange = true;
    
    [_recieptTblView reloadData];
}

- (NSMutableArray *) getDateStringArray {
    
    NSMutableArray *dateStringsArray = [[NSMutableArray alloc] init];
    for(int i=0; i<uniqueDates.count; i++) {
        if(!isLast30Days) {
            if([self checkIfTransactionIsInLast7Days:(NSDate*)[uniqueDates objectAtIndex:i]]) {
                NSDate *transDate = (NSDate*) [uniqueDates objectAtIndex:i];
                NSString *dateStr = [self getShortDateString:transDate];
                [dateStringsArray addObject:dateStr];
            }
        }
        else {
            if([self checkIfTransactionIsInLast30Days:(NSDate*)[uniqueDates objectAtIndex:i]]) {
                NSDate *transDate = (NSDate*) [uniqueDates objectAtIndex:i];
                NSString *dateStr = [self getShortDateString:transDate];
                [dateStringsArray addObject:dateStr];
            }
        }
        
        
    }
    
    return dateStringsArray;
}

- (void) populateLineChart {
    //self.titleLabel.text = @"Line Chart";
    
    self.lineChart = [[PNLineChart alloc] initWithFrame:CGRectMake(0, 45.0, SCREEN_WIDTH, 200.0)];
    self.lineChart.yLabelFormat = @"%1.1f";
    self.lineChart.backgroundColor = [UIColor clearColor];
    [self.lineChart setXLabels:[self getDateStringArray]];
    self.lineChart.showCoordinateAxis = YES;
    
    // added an examle to show how yGridLines can be enabled
    // the color is set to clearColor so that the demo remains the same
    self.lineChart.yGridLinesColor = [UIColor clearColor];
    self.lineChart.showYGridLines = YES;
    
    //Use yFixedValueMax and yFixedValueMin to Fix the Max and Min Y Value
    //Only if you needed
    self.lineChart.yFixedValueMax = [self getHeightAmountFromEntries];
    self.lineChart.yFixedValueMin = 0.0;
    
    [self.lineChart setYLabels:[self generateYAxixIntervals]];
    
    // Line Chart #1
    NSArray * data01Array = [self returnDataValues];;
    PNLineChartData *data01 = [PNLineChartData new];
    data01.dataTitle = @"Alpha";
    data01.color = PNFreshGreen;
    data01.alpha = 0.3f;
    data01.itemCount = data01Array.count;
    data01.inflexionPointColor = PNRed;
    data01.inflexionPointStyle = PNLineChartPointStyleTriangle;
    
    data01.getData = ^(NSUInteger index) {
        CGFloat yValue = [data01Array[index] floatValue];
        return [PNLineChartDataItem dataItemWithY:yValue];
    };
    
    //    // Line Chart #2
    //    NSArray * data02Array = @[@0.0, @180.1, @26.4, @202.2, @126.2, @167.2, @276.2];
    //    PNLineChartData *data02 = [PNLineChartData new];
    //    data02.dataTitle = @"Beta";
    //    data02.color = PNTwitterColor;
    //    data02.alpha = 0.5f;
    //    data02.itemCount = data02Array.count;
    //    data02.inflexionPointStyle = PNLineChartPointStyleCircle;
    //    data02.getData = ^(NSUInteger index) {
    //        CGFloat yValue = [data02Array[index] floatValue];
    //        return [PNLineChartDataItem dataItemWithY:yValue];
    //    };
    
    self.lineChart.chartData = @[data01];
    [self.lineChart strokeChart];
    self.lineChart.delegate = self;
    
    
    [chartView addSubview:self.lineChart];
    
    self.lineChart.legendStyle = PNLegendItemStyleStacked;
    self.lineChart.legendFont = [UIFont boldSystemFontOfSize:12.0f];
    self.lineChart.legendFontColor = [UIColor redColor];
    
    UIView *legend = [self.lineChart getLegendWithMaxWidth:320];
    [legend setFrame:CGRectMake(30, 240, legend.frame.size.width, legend.frame.size.width)];
    [chartView addSubview:legend];
}

- (void)userClickedOnLineKeyPoint:(CGPoint)point lineIndex:(NSInteger)lineIndex pointIndex:(NSInteger)pointIndex{
    NSLog(@"Click Key on line %f, %f line index is %d and point index is %d",point.x, point.y,(int)lineIndex, (int)pointIndex);
}

- (void)userClickedOnLinePoint:(CGPoint)point lineIndex:(NSInteger)lineIndex{
    NSLog(@"Click on line %f, %f, line index is %d",point.x, point.y, (int)lineIndex);
}

- (NSMutableArray *) generateYAxixIntervals {
    
    NSMutableArray *intervalsArray = [[NSMutableArray alloc] init];
    
    int getHeightAmount = [self getHeightAmountFromEntries];
    
    int factorInterval = getHeightAmount/8;
    int returnValue = factorInterval;
    
    for(int i=0; i<8; i++) {
        NSString *factorStr = [NSString stringWithFormat:@"$%d",returnValue];
        returnValue = returnValue+factorInterval;
        
        [intervalsArray addObject:factorStr];
    }
    
    return intervalsArray;
}
- (NSMutableArray *) returnDataValues  {
    
    NSMutableArray *dataArray = [[NSMutableArray alloc] init];
    
    if(transcationsArray.count > 0) {
        for(int i=0; i<transactionDict.count; i++)
        {
            if(!isLast30Days) {
                
                if([self checkIfTransactionIsInLast7Days:[uniqueDates objectAtIndex:i]]) {
                    NSString *keyString = [self getDateFromString : [uniqueDates objectAtIndex:i]];
                    
                    NSArray *tempArrayForDate = [transactionDict objectForKey:keyString];
                    
                    int totalCount = 0;
                    
                    for(int j=0; j<tempArrayForDate.count; j++) {
                        
                        AccountEntry *acountEntryTemp = (AccountEntry*)[tempArrayForDate objectAtIndex:j];
                        if(acountEntryTemp.isPositive) // ignore all negative transactions
                        {
                            totalCount = totalCount + [acountEntryTemp.entryAmount intValue];
                        }
                        
                        
                    }
                    NSString *strAmount = [NSString stringWithFormat:@"%d",totalCount];
                    [dataArray addObject:strAmount];
                }
                
                
            }
            else {
                if([self checkIfTransactionIsInLast30Days:[uniqueDates objectAtIndex:i]]) {
                    NSString *keyString = [self getDateFromString : [uniqueDates objectAtIndex:i]];
                    
                    NSArray *tempArrayForDate = [transactionDict objectForKey:keyString];
                    
                    int totalCount = 0;
                    
                    for(int j=0; j<tempArrayForDate.count; j++) {
                        
                        AccountEntry *acountEntryTemp = (AccountEntry*)[tempArrayForDate objectAtIndex:j];
                        totalCount = totalCount + [acountEntryTemp.entryAmount intValue];
                        
                    }
                    NSString *strAmount = [NSString stringWithFormat:@"%d",totalCount];
                    [dataArray addObject:strAmount];
                }
            }
            
            
        }
        
    }
    return dataArray;
}

- (int) getHeightAmountFromEntries {
    int heighest = 0;
    if(transcationsArray.count > 0) {
        
        for(int i=0; i<transactionDict.count; i++)
        {
            if(!isLast30Days) {
                
                if([self checkIfTransactionIsInLast7Days:[uniqueDates objectAtIndex:i]]) {
                    
                    NSString *keyString = [self getDateFromString : [uniqueDates objectAtIndex:i]];
                    
                    NSArray *tempArrayForDate = [transactionDict objectForKey:keyString];
                    
                    int totalCount = 0;
                    for(int j=0; j<tempArrayForDate.count; j++) {
                        
                        AccountEntry *acountEntryTemp = (AccountEntry*)[tempArrayForDate objectAtIndex:j];
                        if(acountEntryTemp.isPositive) {
                            totalCount = totalCount + [acountEntryTemp.entryAmount intValue];
                        }
                    }
                    if(totalCount > heighest) {
                        heighest = totalCount;
                    }
                }
                
            }
            else {
                if([self checkIfTransactionIsInLast30Days:[uniqueDates objectAtIndex:i]]) {
                    
                    NSString *keyString = [self getDateFromString : [uniqueDates objectAtIndex:i]];
                    
                    NSArray *tempArrayForDate = [transactionDict objectForKey:keyString];
                    
                    int totalCount = 0;
                    for(int j=0; j<tempArrayForDate.count; j++) {
                        
                        AccountEntry *acountEntryTemp = (AccountEntry*)[tempArrayForDate objectAtIndex:j];
                        totalCount = totalCount + [acountEntryTemp.entryAmount intValue];
                        
                    }
                    if(totalCount > heighest) {
                        heighest = totalCount;
                    }
                }
            }
            
        }
        return heighest;
    }
    return 0;
}

- (int) averageTrans {
    int average = 0;
    
    for(int i=0; i<transcationsArray.count; i++) {
        AccountEntry *aModelTemp = [transcationsArray objectAtIndex:i];
        average = average+[aModelTemp.entryAmount intValue];
        
    }
    if(uniqueDates.count > 0) {
        return average/uniqueDates.count;
    }
    
    return average;
}

@end
