//
//  GoalDetailVC.m
//  BankR
//
//  Created by Ahmed Sadiq on 03/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "GoalDetailVC.h"
#import "GoalContributionCell.h"
#import "LDProgressView.h"
#import "AddContributionVC.h"
#import "GoalContributionHeader.h"
#import "GoalContribution.h"
#import "DBManager.h"

#import <QuartzCore/QuartzCore.h>

@interface GoalDetailVC ()

@end

@implementation GoalDetailVC
@synthesize gModel;

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    _contributionArray = [[DBManager getSharedInstance] getAllGoalContributions:[gModel.goalID intValue]];
    [_contributionsTblView reloadData];
    
    int totalContributed = 0;
    for(int i=0; i<_contributionArray.count; i++) {
        GoalContribution *gcModel = (GoalContribution*)[_contributionArray objectAtIndex:i];
        totalContributed = totalContributed + [gcModel.contributionAmount intValue];
    }
    
    gModel.completed = (double)totalContributed / [gModel.targetAmount doubleValue];
    if(gModel.completed > 1) {
        gModel.completed = 1;
    }
    
    LDProgressView *progressView = [[LDProgressView alloc] initWithFrame:CGRectMake(0, 0, 280, 22)];
    progressView.color = [UIColor lightGrayColor];
    progressView.progress = gModel.completed;
    progressView.animate = @YES;
    progressView.type = LDProgressGradient;
    progressView.background = [progressView.color colorWithAlphaComponent:0.8];
    
    [self.progressView addSubview:progressView];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _contributionsTblView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _contributionsTblView.layer.borderWidth = 2.0f;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _contributionArray.count+1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.row == 0) {
        GoalContributionHeader *cell;
        cell= (GoalContributionHeader *)[_contributionsTblView dequeueReusableCellWithIdentifier:nil];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"GoalContributionHeader" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    else {
        GoalContributionCell *cell;
        cell= (GoalContributionCell *)[_contributionsTblView dequeueReusableCellWithIdentifier:nil];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"GoalContributionCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        if(indexPath.row % 2 == 0) {
            cell.contentView.backgroundColor = [UIColor whiteColor];
        }
        else {
            cell.contentView.backgroundColor = [UIColor colorWithRed:0.929 green:0.929 blue:0.929 alpha:1.0];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        GoalContribution *gcModel = [_contributionArray objectAtIndex:(indexPath.row)-1];
        cell.amountLbl.text = gcModel.contributionAmount;
        cell.dateLbl.text = gcModel.contributionDate;
        
        
        return cell;
    }
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0) {
        return 26;
    }
    return 36;
}

- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)plusBtnPressed:(id)sender {
    
    AddContributionVC *contributionController = [[AddContributionVC alloc] initWithNibName:@"AddContributionVC" bundle:nil];
    contributionController.gModel = gModel;
    [self.navigationController pushViewController:contributionController animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
    
}
@end
