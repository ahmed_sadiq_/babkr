//
//  CashflowCell.h
//  BankR
//
//  Created by Ahmed Sadiq on 31/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CashflowCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UILabel *amountLbl;
@end
