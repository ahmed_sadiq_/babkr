//
//  RecieptDetailCell.h
//  BankR
//
//  Created by Ahmed Sadiq on 28/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecieptDetailCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *checkBtn;
@property (strong, nonatomic) IBOutlet UILabel *amount;
@property (strong, nonatomic) IBOutlet UILabel *transType;
@property (strong, nonatomic) IBOutlet UILabel *arrowLbl;
@property (strong, nonatomic) IBOutlet UILabel *subTotal;

@end
