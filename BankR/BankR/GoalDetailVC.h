//
//  GoalDetailVC.h
//  BankR
//
//  Created by Ahmed Sadiq on 03/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoalModel.h"

@interface GoalDetailVC : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *contributionsTblView;
@property (strong, nonatomic) IBOutlet UIView *progressView;
@property (strong, nonatomic) GoalModel *gModel;
@property (strong, nonatomic) NSMutableArray *contributionArray;
- (IBAction)backPressed:(id)sender;
- (IBAction)plusBtnPressed:(id)sender;

@end
