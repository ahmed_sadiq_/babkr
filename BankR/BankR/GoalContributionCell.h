//
//  GoalContributionCell.h
//  BankR
//
//  Created by Ahmed Sadiq on 03/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoalContributionCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *amountLbl;
@property (strong, nonatomic) IBOutlet UILabel *dateLbl;

@end
