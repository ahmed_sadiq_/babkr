//
//  AddBillVC.h
//  BankR
//
//  Created by Ahmed Sadiq on 02/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "NIDropDown.h"
#import "BillModel.h"

@interface AddBillVC : UIViewController <UITextFieldDelegate,NIDropDownDelegate> {
    UIGestureRecognizer *tapper;
    
    NIDropDown *dropDown;
    NSArray * arr;
    IBOutlet UILabel *dueDateLbl;
    IBOutlet UIView *dateView;
    IBOutlet UIDatePicker *datePicker;
    
    BOOL isDateSelected;
    BOOL isRepeat;
    
    BOOL isSpecificDate;
    
    
}


@property (strong, nonatomic) IBOutlet UIView *view1;
@property (strong, nonatomic) IBOutlet UIView *view2;

@property (strong, nonatomic) IBOutlet UITextField *addBillDesc;
@property (strong, nonatomic) IBOutlet UITextField *amountTxt;
@property (strong, nonatomic) IBOutlet UIButton *repeatBtn;

- (IBAction)dateDonePressed:(id)sender;
- (IBAction)backPressed:(id)sender;
- (IBAction)donePressed:(id)sender;
- (IBAction)repeatUntilPressed:(id)sender;
- (IBAction)datePressed:(id)sender;
- (IBAction)repeatPressed:(id)sender;


@end
