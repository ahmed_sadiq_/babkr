//
//  BillTrackerVC.h
//  BankR
//
//  Created by Ahmed Sadiq on 26/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
#import "FSCalendar.h"

@interface BillTrackerVC : UIViewController <UITableViewDelegate,UITableViewDataSource,SWTableViewCellDelegate,FSCalendarDataSource, FSCalendarDelegate> {
    
    IBOutlet UIView *calenderView;
    IBOutlet UIView *calenderInnerView;
    
    
    NSMutableArray *totalBills;
    NSMutableArray *actualTotalBills;
    NSMutableArray *pastBills;
    NSMutableArray *sevenDays;
    NSMutableArray *thirtyDays;
    NSMutableArray *dayBill;
    
}
@property (strong, nonatomic) IBOutlet UITableView *calenderTblView;

@property (weak, nonatomic) FSCalendar *calendar;
@property (weak, nonatomic) UIButton *previousButton;
@property (weak, nonatomic) UIButton *nextButton;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;

@property (strong, nonatomic) NSCalendar *gregorian;

@property (strong, nonatomic) IBOutlet UIButton *doneBtn;
@property (strong, nonatomic) IBOutlet UIButton *optionsBtn;

@property (strong, nonatomic) IBOutlet UITableView *trackingTblView;

- (IBAction)optionsButtonPressed:(id)sender;
- (IBAction)donePressed:(id)sender;
- (IBAction)segmetPressed:(id)sender;
@end
