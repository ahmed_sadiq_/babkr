//
//  DBManager.h
//  SQLite3DBSample
//
//  Created by Ahmed Sadiq on 25/6/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "AccountModel.h"
#import "AccountEntry.h"
#import "CashFlow.h"
#import "CashFlowExpense.h"
#import "BillModel.h"
#import "PaymentModel.h"
#import "GoalModel.h"
#import "GoalContribution.h"

@interface DBManager : NSObject {
    NSString *databasePath;
}
+ (DBManager*)getSharedInstance;

- (BOOL) createDB;
- (NSMutableArray*) getAllAccounts;
- (void) deleteAccount : (int) accountID;
- (void) deleteAllAccounts;
- (BOOL) saveAccountModel:(AccountModel*) accountModel;
- (BOOL) saveAccountEntryModel:(AccountEntry*) accountEntryModel andAccountID : (int) accountID;
- (BOOL) editAccountEntryModel:(AccountEntry*) accountEntryModel;
- (void) deleteAccountEntry : (int) accountEntryID;
- (void) deleteAllAccountEntries : (int) accountID;
- (NSMutableArray*) getAllAccountEntries : (int) accountID;
- (BOOL) saveCashFlow:(CashFlow*) cashFlowModel;
- (void) deleteCashflow : (int) cashflowID;
- (void) deleteAllCashflow;
- (NSMutableArray*) getAllCashflowGroups;

- (BOOL) saveCashFlowExpense:(CashFlowExpense*) cashFlowExpenseModel;
- (void) deleteCashflowExpense : (int) cashFlowExpenseID;
- (void) deleteAllCashflowExpense : (int) cashFlowID;
- (NSMutableArray*) getAllCashflowExpense : (int) cashFlowID;
- (BOOL) editCashFlowExpenseModel:(CashFlowExpense*) cfModel;

- (BOOL) saveBill:(BillModel*) bModel;
- (void) deleteBill : (int) billID;
- (void) deleteAllBills;
- (NSMutableArray*) getAllBills;

- (BOOL) savePayment:(PaymentModel*) pModel;
- (void) deletePayment : (int) paymentID;
- (void) deleteAllPayments;
- (NSMutableArray*) getAllPayments : (NSString *) billID;

- (BOOL) saveGoal:(GoalModel*) gModel;
- (void) deleteGoal : (int) goalID;
- (NSMutableArray*) getAllGoals;

- (BOOL) saveGoalContribution:(GoalContribution*) gcModel;
- (NSMutableArray*) getAllGoalContributions : (int) goalID;
@end
