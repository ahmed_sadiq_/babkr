//
//  GoalsVC.m
//  BankR
//
//  Created by Ahmed Sadiq on 26/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "GoalsVC.h"
#import "GoalsCell.h"
#import "AddGoalVC.h"
#import "GoalDetailVC.h"
#import "LDProgressView.h"
#import "DBManager.h"
#import "GoalModel.h"
#import "GoalContribution.h"

@interface GoalsVC ()

@end

@implementation GoalsVC

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    _goalsArray = [[DBManager getSharedInstance] getAllGoals];
    [self depriveInProgressGoals];
    [_goalsTblview reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if(section == 0) {
        return @"IN PROGRESS";
    }
    else {
        return @"COMPLETED GOALS";
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0) {
        return _inProgressArray.count;;
    }
    else {
        return _completedArray.count;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"GoalsCell";
    GoalsCell *cell = (GoalsCell *)[_goalsTblview dequeueReusableCellWithIdentifier:nil];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"GoalsCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    GoalModel *gModel;
    
    if(indexPath.section == 0) {
        gModel = [_inProgressArray objectAtIndex:indexPath.row];
    }
    else {
        gModel = [_completedArray objectAtIndex:indexPath.row];
    }
    
    cell.savingTxt.text = gModel.goalName;
    
    if(gModel.completed > 1) {
        gModel.completed = 1;
    }
    
    LDProgressView *progressView = [[LDProgressView alloc] initWithFrame:CGRectMake(0, 0, 280, 22)];
    progressView.color = [UIColor lightGrayColor];
    progressView.progress = gModel.completed;
    progressView.animate = @YES;
    progressView.type = LDProgressGradient;
    progressView.background = [progressView.color colorWithAlphaComponent:0.8];
    
    [cell.progressView addSubview:progressView];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    GoalModel *gModel;
    
    if(indexPath.section == 0) {
        gModel = [_inProgressArray objectAtIndex:indexPath.row];
    }
    else {
        gModel = [_completedArray objectAtIndex:indexPath.row];
    }
    
    GoalDetailVC *editRecieptVC = [[GoalDetailVC alloc] initWithNibName:@"GoalDetailVC" bundle:nil];
    editRecieptVC.gModel = gModel;
    [self.navigationController pushViewController:editRecieptVC animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
}
- (UITableViewCellEditingStyle) tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleDelete;
}

- (BOOL) tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void) tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
}


- (IBAction)optionsPressed:(id)sender {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    actionSheet.view.tintColor = [UIColor colorWithRed:0.549 green:0.549 blue:0.549 alpha:1.0];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped do nothing.
        
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Add Goal" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        AddGoalVC *editRecieptVC = [[AddGoalVC alloc] initWithNibName:@"AddGoalVC" bundle:nil];
        [self.navigationController pushViewController:editRecieptVC animated:YES];
        [self.navigationController setNavigationBarHidden:YES];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Rearrange Goals" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        _optionsBtn.hidden = TRUE;
        _doneBtn.hidden = FALSE;
        
        [_goalsTblview setEditing:TRUE animated:YES];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Delete Goals" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        _optionsBtn.hidden = TRUE;
        _doneBtn.hidden = FALSE;
        
        [_goalsTblview setEditing:TRUE animated:YES];
        
    }]];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (IBAction)doneBtnPressed:(id)sender {
    
    [_goalsTblview setEditing:false animated:YES];
    
    _optionsBtn.hidden = false;
    _doneBtn.hidden = true;
}

- (void) depriveInProgressGoals {
    _inProgressArray = [[NSMutableArray alloc] init];
    _completedArray = [[NSMutableArray alloc] init];
    for(int i=0; i<_goalsArray.count; i++) {
        GoalModel *gModel = (GoalModel*)[_goalsArray objectAtIndex:i];
        NSMutableArray *contributionsForGoal = [[DBManager getSharedInstance] getAllGoalContributions:[gModel.goalID intValue]];
        if([self ifTargetAchieved:contributionsForGoal andTarget:gModel]) {
            [_completedArray addObject:gModel];
        }
        else {
            [_inProgressArray addObject:gModel];
        }
    }
}

- (BOOL) ifTargetAchieved : (NSMutableArray *)contributionArray andTarget : (GoalModel*) gModel {
    int totalContributed = 0;
    for(int i=0; i<contributionArray.count; i++) {
        GoalContribution *gcModel = (GoalContribution*)[contributionArray objectAtIndex:i];
        totalContributed = totalContributed + [gcModel.contributionAmount intValue];
    }
    
    gModel.completed = (double)totalContributed / [gModel.targetAmount doubleValue];
    
    if(totalContributed >= [gModel.targetAmount intValue]) {
        return true;
    }
    
    return false;
}
@end
