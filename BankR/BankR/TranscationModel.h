//
//  TranscationModel.h
//  BankR
//
//  Created by Ahmed Sadiq on 27/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TranscationModel : NSObject

@property BOOL isTranscationCleared;
@property BOOL isNegative;
@property (strong, nonatomic) NSString *bankID;
@property (strong, nonatomic) NSString *bankName;
@property (strong, nonatomic) NSString *transactionID;
@property (strong, nonatomic) NSString *transactionDate;
@property (strong, nonatomic) NSString *transactionType;
@property (strong, nonatomic) NSString *transactionAmount;

@property BOOL isHeader;
@property (strong, nonatomic) NSString *headerTitle;

@end
