//
//  AddAccountVC.h
//  BankR
//
//  Created by Ahmed Sadiq on 27/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface AddAccountVC : UIViewController <UITextFieldDelegate> {
    
    IBOutlet UISegmentedControl *segmetCont;
    BOOL isNegative;
    UIGestureRecognizer *tapper;
    IBOutlet UIView *view1;
    IBOutlet UIView *view2;
    
    IBOutlet UITextField *accountNumTxt;
    IBOutlet UITextField *startingBalanceTxt;
}
- (IBAction)backPressed:(id)sender;
- (IBAction)donePressed:(id)sender;

- (IBAction)segmetPressed:(id)sender;
@end
