//
//  BillTrackerVC.m
//  BankR
//
//  Created by Ahmed Sadiq on 26/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "BillTrackerVC.h"
#import "BillCell.h"
#import "BillDetailVC.h"
#import "AddBillVC.h"
#import "DBManager.h"
#import "BillModel.h"

@interface BillTrackerVC ()

@end

@implementation BillTrackerVC

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    totalBills = [[DBManager getSharedInstance] getAllBills];
    actualTotalBills = [[DBManager getSharedInstance] getAllBills];
    [self getAllPostDueBills];
    [self getSevenDaysDue];
    [self getThirtyDaysDue];
    
    [_trackingTblView reloadData];
    
    NSArray *viewsToRemove = [calenderInnerView subviews];
    for (UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
    [self setCalender];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
}

- (void) getAllPostDueBills {
    pastBills = [[NSMutableArray alloc] init];
    
    for(int i=0; i<totalBills.count; i++) {
        BillModel *bModel = [totalBills objectAtIndex:i];
        NSDate *dueDate = [self getStringFromDate:bModel.dueDate];
        NSDateComponents *components1 = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:dueDate];
        
        NSDateComponents *components2 = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
        
        BOOL isdatePost = [self isDatePast:components1 andComponent2:components2 andIsRepeat:bModel.isRepeat];
        if(isdatePost) {
            [pastBills addObject:bModel];
        }
    }
}

- (void) getSevenDaysDue {
    sevenDays = [[NSMutableArray alloc] init];
    
    for(int i=0; i<totalBills.count; i++) {
        BillModel *bModel = [totalBills objectAtIndex:i];
        NSDate *dueDate = [self getStringFromDate:bModel.dueDate];
        
        NSString *combo = [NSString stringWithFormat:@"%@%@",bModel.billID,[self getDateFromStringForCombo:[NSDate date]]];
        BOOL isPaymentDoneForMonth = [[NSUserDefaults standardUserDefaults] boolForKey:combo];
        
        if(!isPaymentDoneForMonth) {
            BOOL isTokenValid;
            if(![bModel.repeatUntil isEqualToString:@" Forever"]) {
                NSDate *specificDate = [self getStringFromDate:bModel.repeatUntil];
                
                if ([specificDate compare:[NSDate date]] == NSOrderedDescending) {
                    isTokenValid = true;
                }
                else {
                    isTokenValid = false;
                }
                
            }
            else {
                isTokenValid = true;
            }
            
            if(isTokenValid) {
                NSDateComponents *components1 = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:dueDate];
                
                NSDateComponents *components2 = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
                
                BOOL isdatePost = [self isDateDueSevenDays:components1 andComponent2:components2 andIsRepeat:bModel.isRepeat];
                
                if(isdatePost) {
                    [sevenDays addObject:bModel];
                }
            }
        }
        
        
    }
}

- (void) getThirtyDaysDue {
    thirtyDays = [[NSMutableArray alloc] init];
    
    for(int i=0; i<totalBills.count; i++) {
        BillModel *bModel = [totalBills objectAtIndex:i];
        NSDate *dueDate = [self getStringFromDate:bModel.dueDate];
        
        NSString *combo = [NSString stringWithFormat:@"%@%@",bModel.billID,[self getDateFromStringForCombo:[NSDate date]]];
        BOOL isPaymentDoneForMonth = [[NSUserDefaults standardUserDefaults] boolForKey:combo];
        
        if(!isPaymentDoneForMonth) {
            BOOL isTokenValid;
            if(![bModel.repeatUntil isEqualToString:@" Forever"]) {
                NSDate *specificDate = [self getStringFromDate:bModel.repeatUntil];
                
                if ([specificDate compare:[NSDate date]] == NSOrderedDescending) {
                    isTokenValid = true;
                }
                else {
                    isTokenValid = false;
                }
                
            }
            else {
                isTokenValid = true;
            }
            
            if(isTokenValid) {
                NSDateComponents *components1 = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:dueDate];
                
                NSDateComponents *components2 = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
                
                BOOL isdatePost = [self isDateDueThirtyDays:components1 andComponent2:components2 andIsRepeat:bModel.isRepeat];
                if(isdatePost ) {
                    [thirtyDays addObject:bModel];
                }
            }
        }
        
    }
}

- (BOOL) isDatePast : (NSDateComponents *) component1 andComponent2 :(NSDateComponents*) component2 andIsRepeat:(BOOL) isRepeat {
    
    NSInteger day1 = [component1 day];
    NSInteger month1 = [component1 month];
    
    NSInteger day2 = [component2 day];
    NSInteger month2 = [component2 month];
    
    //Check if it is repeating or not, if not repeating than check if year is same or not
    
    if(isRepeat) {
        if(day1 < day2 ) {
            return true;
        }
    }
    else {
        NSInteger year1 = [component1 year];
        NSInteger year2 = [component2 year];
        
        if (month1 == month2 && year1 == year2) {
            
            if(day1 < day2 ) {
                return true;
            }
        }
        
    }
    
    
    
    return false;
}

- (BOOL) isDateDueSevenDays : (NSDateComponents *) component1 andComponent2 :(NSDateComponents*) component2 andIsRepeat:(BOOL) isRepeat {
    
    NSInteger day1 = [component1 day];
    NSInteger month1 = [component1 month];
    
    NSInteger day2 = [component2 day];
    NSInteger month2 = [component2 month];
    
    // if repeating than check if date is due in next 7 days and if not repeating than check if year is same and than date is for next 7 days
    
    if(isRepeat) {
        if(day1-day2 >=0 && day1-day2 < 7) {
            return true;
        }
    }
    else {
        NSInteger year1 = [component1 year];
        NSInteger year2 = [component2 year];
        
        if(month2==month1 && day1-day2 >=0 && day1-day2 <7 && year1 == year2) {
            return true;
        }
    }
    
    return false;
}

- (BOOL) isDateDueThirtyDays : (NSDateComponents *) component1 andComponent2 :(NSDateComponents*) component2 andIsRepeat : (BOOL) isRepeat {
    
    NSInteger day1 = [component1 day];
    NSInteger month1 = [component1 month];
    
    NSInteger day2 = [component2 day];
    NSInteger month2 = [component2 month];
    
    // if repeating than check if date is due in next 7 days and if not repeating than check if year is same and than date is for next 7 days
    
    if(isRepeat) {
        if(day1-day2 >=7 && day1-day2 < 30) {
            return true;
        }
    }
    else {
        NSInteger year1 = [component1 year];
        NSInteger year2 = [component2 year];
        
        if(month2==month1 && day1-day2 >=7 && day1-day2 < 30 && year1 == year2) {
            return true;
        }
    }
    
    return false;
}

- (BOOL) isDateSameDay : (NSDateComponents *) component1 andComponent2 :(NSDateComponents*) component2 andIsRepeat : (BOOL) isRepeat {
    
    NSInteger day = [component1 day];
    NSInteger month = [component1 month];
    
    NSInteger day1 = [component2 day];
    NSInteger month1 = [component2 month];
    
    if(isRepeat) {
        if(day1 == day) {
            return true;
        }
    }
    else {
        NSInteger year = [component1 year];
        NSInteger year1 = [component2 year];
        
        if (month1 == month && day == day1 && year == year1) {
            return true;
        }
    }
    return false;
}

-(NSString *)getDayFromString:(NSDate *)pstrDate
{
    NSDateFormatter* myFormatter = [[NSDateFormatter alloc] init];
    [myFormatter setDateFormat:@"dd"];
    NSString* myDateStr = [myFormatter stringFromDate:pstrDate];
    return myDateStr;
}

-(NSString *)getMonthFromString:(NSDate *)pstrDate
{
    NSDateFormatter* myFormatter = [[NSDateFormatter alloc] init];
    [myFormatter setDateFormat:@"MMM"];
    NSString* myDateStr = [myFormatter stringFromDate:pstrDate];
    return myDateStr;
}

-(NSString *)getDateFromString:(NSDate *)pstrDate
{
    NSDateFormatter* myFormatter = [[NSDateFormatter alloc] init];
    [myFormatter setDateFormat:@"dd MMM"];
    NSString* myDateStr = [myFormatter stringFromDate:pstrDate];
    return myDateStr;
}

-(NSString *)getDateFromStringForCombo:(NSDate *)pstrDate
{
    NSDateFormatter* myFormatter = [[NSDateFormatter alloc] init];
    [myFormatter setDateFormat:@"MMM dd, yyyy"];
    NSString* myDateStr = [myFormatter stringFromDate:pstrDate];
    return myDateStr;
}

- (NSDate *)getStringFromDate:(NSString *)pstrDate
{
    NSDateFormatter* myFormatter = [[NSDateFormatter alloc] init];
    [myFormatter setDateFormat:@"MM-dd-yyyy"];
    NSDate* myDate = [myFormatter dateFromString:pstrDate];
    return myDate;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView.tag == 2) {
        return 1;
    }
    return 3;    //count of section
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if(tableView.tag == 2) {
        return nil;
    }
    else if(section == 0)
    {
        return @"PAST DUE";
    }
    else if (section == 1) {
        return @"DUE WITHIN 7 DAYS";
    }
    else {
        return @"DUE WITHIN 30 DAYS";
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(tableView.tag == 2) {
        return dayBill.count;
    }
    else if(section == 0)
    {
        return pastBills.count;
    }
    else if (section == 1) {
        return sevenDays.count;
    }
    else {
        return thirtyDays.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView.tag == 2) {
        
        BillCell *cell = (BillCell *)[_calenderTblView dequeueReusableCellWithIdentifier:nil];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"BillCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        BillModel *bModel;
        bModel = [dayBill objectAtIndex:indexPath.row];
        NSDate *dateTemp = [self getStringFromDate:bModel.dueDate];
        
        cell.amount.text = [NSString stringWithFormat:@"$%@",bModel.amount];
        cell.desc.text = [NSString stringWithFormat:@"%@",bModel.billDesc];
        cell.dueDate.text = [NSString stringWithFormat:@"due on %@ %@",[self getDayFromString:dateTemp],[self getMonthFromString:[NSDate date]]];

        
        return cell;
    }
    else {
        
        BillCell *cell = (BillCell *)[_trackingTblView dequeueReusableCellWithIdentifier:nil];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"BillCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        [cell setRightUtilityButtons:[self rightButtons:indexPath.section andIndex:indexPath.row ] WithButtonWidth:58.0f];
        
        cell.delegate = self;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        BillModel *bModel;
        if(indexPath.section == 0) {
            bModel = [pastBills objectAtIndex:indexPath.row];
        }
        else if (indexPath.section == 1) {
            bModel = [sevenDays objectAtIndex:indexPath.row];
        }
        else {
            bModel = [thirtyDays objectAtIndex:indexPath.row];
        }
        
        cell.tag = indexPath.row;
        cell.accessibilityHint = [NSString stringWithFormat:@"%d",indexPath.section];
        
        NSDate *dateTemp = [self getStringFromDate:bModel.dueDate];
        cell.amount.text = [NSString stringWithFormat:@"$%@",bModel.amount];
        cell.desc.text = [NSString stringWithFormat:@"%@",bModel.billDesc];
        cell.dueDate.text = [NSString stringWithFormat:@"due on %@ %@",[self getDayFromString:dateTemp],[self getMonthFromString:[NSDate date]]];
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag ==0) {
        
        BillModel *bModel;
        if(indexPath.section == 0) {
            bModel = [pastBills objectAtIndex:indexPath.row];
            return;
        }
        else if (indexPath.section == 1) {
            bModel = [sevenDays objectAtIndex:indexPath.row];
        }
        else {
            bModel = [thirtyDays objectAtIndex:indexPath.row];
        }
        
        
        BillDetailVC *billDetailVC = [[BillDetailVC alloc] initWithNibName:@"BillDetailVC" bundle:nil];
        billDetailVC.bModel =bModel;
        [self.navigationController pushViewController:billDetailVC animated:YES];
        [self.navigationController setNavigationBarHidden:YES];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (NSArray *)rightButtons : (int) section andIndex : (int) index
{
    NSMutableArray *leftUtilityButtons = [NSMutableArray new];
    
    [leftUtilityButtons sw_addUtilityButtonWithColor: [UIColor clearColor] icon:[UIImage imageNamed:@"quick_pay.png"]];
    
    return leftUtilityButtons;
}

#pragma mark - SWTableViewDelegate

- (void)swipeableTableViewCell:(SWTableViewCell *)cell scrollingToState:(SWCellState)state
{
    switch (state) {
        case 0:
            NSLog(@"utility buttons closed");
            break;
        case 1:
            NSLog(@"left utility buttons open");
            break;
        case 2:
            NSLog(@"right utility buttons open");
            break;
        default:
            break;
    }
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index
{
    switch (index) {
        case 0:
            NSLog(@"left button 0 was pressed");
            break;
        case 1:
            NSLog(@"left button 1 was pressed");
            break;
        case 2:
            NSLog(@"left button 2 was pressed");
            break;
        case 3:
            NSLog(@"left btton 3 was pressed");
        default:
            break;
    }
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    switch (index) {
        case 0:
        {
            
            int section = [cell.accessibilityHint intValue];
            int index = cell.tag;
            
            
            [cell hideUtilityButtonsAnimated:YES];
            
            BillModel *bModel;
            if(section == 0) {
                bModel = [pastBills objectAtIndex:index];
                
            }
            else if (section == 1) {
                bModel = [sevenDays objectAtIndex:index];
            }else {
                bModel = [thirtyDays objectAtIndex:index];
            }
            //[self getDateFromStringForCombo:[NSDate date]
            NSString *combo = [NSString stringWithFormat:@"%@%@",bModel.billID,[self getDateFromStringForCombo:[NSDate date]]];
            
            [[NSUserDefaults standardUserDefaults] setBool:true forKey:combo];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            totalBills = [[DBManager getSharedInstance] getAllBills];
            actualTotalBills = [[DBManager getSharedInstance] getAllBills];
            [self getAllPostDueBills];
            [self getSevenDaysDue];
            [self getThirtyDaysDue];
            
            [_trackingTblView reloadData];
            
            NSArray *viewsToRemove = [calenderInnerView subviews];
            for (UIView *v in viewsToRemove) {
                [v removeFromSuperview];
            }
            
            [self setCalender];
            
            [_trackingTblView reloadData];
            
            break;
        }
        case 1:
        {
            
            break;
        }
        default:
            break;
    }
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    // allow just one cell's utility button to be open at once
    return YES;
}

- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state
{
    switch (state) {
        case 1:
            // set to NO to disable all left utility buttons appearing
            return YES;
            break;
        case 2:
            // set to NO to disable all right utility buttons appearing
            return YES;
            break;
        default:
            break;
    }
    
    return YES;
}
- (IBAction)optionsButtonPressed:(id)sender {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    actionSheet.view.tintColor = [UIColor colorWithRed:0.549 green:0.549 blue:0.549 alpha:1.0];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped do nothing.
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Add Bill" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        AddBillVC *addBillVC = [[AddBillVC alloc] initWithNibName:@"AddBillVC" bundle:nil];
        
        [self.navigationController pushViewController:addBillVC animated:YES];
        [self.navigationController setNavigationBarHidden:YES];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Delete Bill" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        _optionsBtn.hidden = true;
        _doneBtn.hidden = false;
        
        [_trackingTblView setEditing:true animated:YES];
        
    }]];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

- (IBAction)donePressed:(id)sender {
    _optionsBtn.hidden = false;
    _doneBtn.hidden = true;
    [_trackingTblView setEditing:false animated:YES];
}

- (IBAction)segmetPressed:(id)sender {
    UISegmentedControl *segCont = (UISegmentedControl*) sender;
    
    if(segCont.selectedSegmentIndex == 0) {
        _trackingTblView.hidden = false;
        calenderView.hidden = true;
    }
    else {
        _trackingTblView.hidden = true;
        calenderView.hidden = false;
    }
    
}

- (void) setCalender {
    // Do any additional setup after loading the view from its nib.
    self.gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    // border
    [calenderInnerView.layer setBorderColor:[UIColor darkGrayColor].CGColor];
    [calenderInnerView.layer setBorderWidth:1.5f];
    calenderInnerView.layer.masksToBounds = true;
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    self.dateFormatter.dateFormat = @"yyyy-MM-dd";
    
    
    // 450 for iPad and 300 for iPhone
    CGFloat height = calenderInnerView.frame.size.height;
    FSCalendar *calendar = [[FSCalendar alloc] initWithFrame:CGRectMake(0, 0, calenderInnerView.frame.size.width, height)];
    calendar.dataSource = self;
    calendar.delegate = self;
    calendar.backgroundColor = [UIColor clearColor];
    calendar.appearance.headerMinimumDissolvedAlpha = 0;
    calendar.appearance.caseOptions = FSCalendarCaseOptionsHeaderUsesUpperCase;
    [calenderInnerView addSubview:calendar];
    self.calendar = calendar;
    
    UIButton *previousButton = [UIButton buttonWithType:UIButtonTypeCustom];
    previousButton.frame = CGRectMake(-20, 5, 95, 34);
    previousButton.backgroundColor = [UIColor clearColor];
    previousButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [previousButton setImage:[UIImage imageNamed:@"icon_prev"] forState:UIControlStateNormal];
    [previousButton addTarget:self action:@selector(previousClicked:) forControlEvents:UIControlEventTouchUpInside];
    [calenderInnerView addSubview:previousButton];
    self.previousButton = previousButton;
    
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    nextButton.frame = CGRectMake(CGRectGetWidth(calenderInnerView.frame)-75, 5, 95, 34);
    nextButton.backgroundColor = [UIColor clearColor];
    nextButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [nextButton setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(nextClicked:) forControlEvents:UIControlEventTouchUpInside];
    [calenderInnerView addSubview:nextButton];
    self.nextButton = nextButton;
    
    [self setUpCalenderBills:[NSDate date]];
}

- (void)previousClicked:(id)sender
{
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *previousMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:previousMonth animated:YES];
}

- (void)nextClicked:(id)sender
{
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *nextMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:nextMonth animated:YES];
}

- (void) setUpCalenderBills : (NSDate*)dateToBeExectued {
    dayBill = [[NSMutableArray alloc] init];
    
    for(int i=0; i<actualTotalBills.count; i++) {
        BillModel *bModel = [actualTotalBills objectAtIndex:i];
        NSDate *dueDate = [self getStringFromDate:bModel.dueDate];
        
        
        NSDateComponents *components1 = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:dueDate];
        
        NSDateComponents *components2 = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:dateToBeExectued];
        
        BOOL isdatePost = [self isDateSameDay:components1 andComponent2:components2 andIsRepeat:bModel.isRepeat];
        if(isdatePost ) {
            [dayBill addObject:bModel];
        }
    }
    
    [_calenderTblView reloadData];
}
- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date {
    [self setUpCalenderBills:date];
}

@end
