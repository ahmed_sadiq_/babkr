//
//  CashFlowGroupDetailsVC.h
//  BankR
//
//  Created by Ahmed Sadiq on 31/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CashFlow.h"

typedef enum{ Plus,Minus,Multiply,Divide} CalcOperation;

@interface CashFlowGroupDetailsVC : UIViewController <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    NSMutableArray *transcationsArray;
    IBOutlet UITableView *mainTblView;
    BOOL isRearrange;
    IBOutlet UIButton *doneBtn;
    IBOutlet UILabel *totalAmount;
    UIGestureRecognizer *tapper;
    
    
    int subTotal;
    IBOutlet UILabel *ttlLbl;
    
    IBOutlet UIView *newExpense;
    
    IBOutlet UITextField *descTxt;
    IBOutlet UITextField *amountTxt;
    IBOutlet UIButton *optionBtn;
    
    IBOutlet UITextField *display;
    IBOutlet UIButton *cbutton;
    NSString *storage;
    CalcOperation operation;
    IBOutlet UIView *calculatorView;
    IBOutlet UISegmentedControl *segmentCont;
    
    BOOL isNegative;
}

@property (strong, nonatomic) CashFlow *cModel;


- (IBAction)doneBtnPressed:(id)sender;
- (IBAction)calculatorPressed:(id)sender;

- (IBAction)expenseCancelPressed:(id)sender;
- (IBAction)expenseSavePressed:(id)sender;
- (IBAction)valueChangedPressed:(id)sender;
@end
