//
//  Utils.h
//  Joumana
//
//  Created by Ahmed Sadiq on 18/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Utils : NSObject

+(UIAlertController *) showCustomAlert:(NSString *) title andMessage : (NSString *) message;
+ (NSData*)encodeDictionary:(NSDictionary*)dictionary;
+ (BOOL)validateEmail:(NSString *)candidate;
@end
