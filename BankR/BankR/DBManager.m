#import "DBManager.h"
static DBManager *sharedInstance = nil;
static sqlite3 *database = nil;
static sqlite3_stmt *statement = nil;

@implementation DBManager

#pragma mark - Database Configuration Methods

+(DBManager*)getSharedInstance{
    if (!sharedInstance) {
        sharedInstance = [[super allocWithZone:NULL]init];
        [sharedInstance createDB];
    }
    return sharedInstance;
}

-(BOOL)createDB{
    NSString *docsDir;
    NSArray *dirPaths;
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString:
                    [docsDir stringByAppendingPathComponent: @"bankr.db"]];
    BOOL isSuccess = YES;
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            char *errMsg;
            const char *sql_stmt = "create table if not exists account (accountID integer primary key, accountName text, accountNumber text, startingBalance text, isPositive integer)";
            if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg)
                != SQLITE_OK)
            {
                isSuccess = NO;
                NSLog(@"Failed to create table");
            }
            
            const char *sql_stmt_Account_enrty = "create table if not exists accountEntry (accountEntryID integer primary key, accountID integer, entryDesc text, entryAmount text, entryDate text, isPositive integer, isCleared integer)";
            if (sqlite3_exec(database, sql_stmt_Account_enrty, NULL, NULL, &errMsg)
                != SQLITE_OK)
            {
                isSuccess = NO;
                NSLog(@"Failed to create table");
            }
            
            const char *sql_stmt_Cashflow = "create table if not exists cashflow (cashflowID integer primary key, groupName text, totalBalance text)";
            if (sqlite3_exec(database, sql_stmt_Cashflow, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                isSuccess = NO;
                NSLog(@"Failed to create table");
            }
            
            const char *sql_stmt_CashflowExpense = "create table if not exists cashflowexpense (expenseID integer primary key, cashflowID integer, expenseDesc text, expenseAmount text,isCleared integer, isPositive integer,  headerID integer,  isHeader integer, headerTitle text)";
            if (sqlite3_exec(database, sql_stmt_CashflowExpense, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                isSuccess = NO;
                NSLog(@"Failed to create table");
            }
            
            const char *sql_stmt_BillModel = "create table if not exists bill (billID integer primary key, billDesc text, billAmount text, dueDate text,isRepeat integer, repeatUntil text)";
            if (sqlite3_exec(database, sql_stmt_BillModel, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                isSuccess = NO;
                NSLog(@"Failed to create table");
            }
            
            const char *sql_stmt_Payment = "create table if not exists payment (paymentID integer primary key, billID integer ,billAmount text, datePaid text)";
            if (sqlite3_exec(database, sql_stmt_Payment, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                isSuccess = NO;
                NSLog(@"Failed to create table");
            }
            
            const char *sql_stmt_Goal = "create table if not exists goal (goalID integer primary key ,goalName text, target text)";
            if (sqlite3_exec(database, sql_stmt_Goal, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                isSuccess = NO;
                NSLog(@"Failed to create table");
            }
            
            const char *sql_stmt_GoalContribution = "create table if not exists goalContribution (contribtionID integer primary key ,goalID integer ,contributionAmount text ,contributionDate text)";
            if (sqlite3_exec(database, sql_stmt_GoalContribution, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                isSuccess = NO;
                NSLog(@"Failed to create table");
            }
            
            sqlite3_close(database);
            return  isSuccess;
        }
        else {
            isSuccess = NO;
            NSLog(@"Failed to open/create database");
        }
    }
    return isSuccess;
}

#pragma mark - Goal Contribution Methods

-(BOOL) saveGoalContribution:(GoalContribution*) gcModel {
    
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"insert into goalContribution (goalID,contributionAmount,contributionDate) values (\"%d\",\"%@\",\"%@\")",gcModel.goalID, gcModel.contributionAmount, gcModel.contributionDate];
        
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE){
            return YES;
        }
        else {
            return NO;
        }
        
        sqlite3_reset(statement);
        
    }
    return NO;
}

//const char *sql_stmt_GoalContribution = "create table if not exists goalContribution (contribtionID integer primary key ,goalID integer ,contributionAmount text ,contributionDate text)";

- (NSMutableArray*) getAllGoalContributions : (int) goalID
{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat: @"select * from goalContribution where goalID = %d",goalID];
        const char *query_stmt = [querySQL UTF8String];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        
        if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                
                GoalContribution *gcModel = [[GoalContribution alloc] init];
                
                gcModel.contributionID = [[[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)] intValue];
                
                gcModel.goalID = [[[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 1)] intValue];
                
                gcModel.contributionAmount = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 2)];
                
                gcModel.contributionDate = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 3)];
                
                [resultArray addObject:gcModel];
            }
            
            sqlite3_reset(statement);
            return resultArray;
        }
    }
    return nil;
}

#pragma mark - Goal Methods

-(BOOL) saveGoal:(GoalModel*) gModel {
    
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"insert into goal (goalName, target) values (\"%@\",\"%@\")",gModel.goalName,gModel.targetAmount];
        
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE){
            return YES;
        }
        else {
            return NO;
        }
        
        sqlite3_reset(statement);
        
    }
    return NO;
}

- (void) deleteGoal : (int) goalID
{
    NSString *sql_str=[NSString stringWithFormat:@"DELETE FROM goal where goalID = %d",goalID];
    const char *sql = [sql_str UTF8String];
    
    sqlite3 *database;
    const char *dbpath = [databasePath UTF8String];
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        sqlite3_stmt *deleteStmt;
        if(sqlite3_prepare_v2(database, sql, -1, &deleteStmt, NULL) == SQLITE_OK)
        {
            
            if(sqlite3_step(deleteStmt) != SQLITE_DONE )
            {
                NSLog( @"Error: %s", sqlite3_errmsg(database) );
            }
            else
            {
                NSLog(@"No Error");
            }
        }
        sqlite3_finalize(deleteStmt);
    }
    sqlite3_close(database);
}

- (NSMutableArray*) getAllGoals
{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat: @"select * from goal"];
        const char *query_stmt = [querySQL UTF8String];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        
        if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                
                GoalModel *gModel = [[GoalModel alloc] init];
                
                gModel.goalID = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
                
                gModel.goalName = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 1)];
                
                gModel.targetAmount = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 2)];
                
                
                [resultArray addObject:gModel];
            }
            
            sqlite3_reset(statement);
            return resultArray;
        }
    }
    return nil;
}

#pragma mark - Payment Expense Methods

-(BOOL) savePayment:(PaymentModel*) pModel {
    
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"insert into payment (billID, billAmount,datePaid) values (\"%d\", \"%@\", \"%@\")",[pModel.billID intValue], pModel.amount, pModel.paymentDate];
        
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE){
            return YES;
        }
        else {
            return NO;
        }
        
        sqlite3_reset(statement);
        
    }
    return NO;
}

- (void) deletePayment : (int) paymentID
{
    NSString *sql_str=[NSString stringWithFormat:@"DELETE FROM payment where paymentID = %d",paymentID];
    const char *sql = [sql_str UTF8String];
    
    sqlite3 *database;
    const char *dbpath = [databasePath UTF8String];
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        sqlite3_stmt *deleteStmt;
        if(sqlite3_prepare_v2(database, sql, -1, &deleteStmt, NULL) == SQLITE_OK)
        {
            
            if(sqlite3_step(deleteStmt) != SQLITE_DONE )
            {
                NSLog( @"Error: %s", sqlite3_errmsg(database) );
            }
            else
            {
                NSLog(@"No Error");
            }
        }
        sqlite3_finalize(deleteStmt);
    }
    sqlite3_close(database);
}

- (void) deleteAllPayments
{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat: @"DELETE from payment"];
        const char *query_stmt = [querySQL UTF8String];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        
        if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                
            }
            sqlite3_reset(statement);
        }
    }
}

- (NSMutableArray*) getAllPayments : (NSString *) billID
{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat: @"select * from payment where billID = %@",billID];
        const char *query_stmt = [querySQL UTF8String];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        
        if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                
                PaymentModel *pModel = [[PaymentModel alloc] init];
                
                pModel.paymentID = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
                
                pModel.billID = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 1)];
                
                pModel.amount = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 2)];
                
                pModel.paymentDate = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 3)];
                
                [resultArray addObject:pModel];
            }
            
            sqlite3_reset(statement);
            return resultArray;
        }
    }
    return nil;
}

#pragma mark - Bills Expense Methods

-(BOOL) saveBill:(BillModel*) bModel {
    
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"insert into bill (billDesc, billAmount,dueDate,isRepeat,repeatUntil) values (\"%@\", \"%@\", \"%@\", \"%d\", \"%@\")",bModel.billDesc, bModel.amount, bModel.dueDate, bModel.isRepeat, bModel.repeatUntil];
        
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            return YES;
        }
        else {
            return NO;
        }
        
        sqlite3_reset(statement);
        
    }
    return NO;
}

- (void) deleteBill : (int) billID
{
    NSString *sql_str=[NSString stringWithFormat:@"DELETE FROM bill where billID = %d",billID];
    const char *sql = [sql_str UTF8String];
    
    sqlite3 *database;
    const char *dbpath = [databasePath UTF8String];
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        sqlite3_stmt *deleteStmt;
        if(sqlite3_prepare_v2(database, sql, -1, &deleteStmt, NULL) == SQLITE_OK)
        {
            
            if(sqlite3_step(deleteStmt) != SQLITE_DONE )
            {
                NSLog( @"Error: %s", sqlite3_errmsg(database) );
            }
            else
            {
                NSLog(@"No Error");
            }
        }
        sqlite3_finalize(deleteStmt);
    }
    sqlite3_close(database);
}

- (void) deleteAllBills
{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat: @"DELETE from bill"];
        const char *query_stmt = [querySQL UTF8String];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        
        if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                
            }
            sqlite3_reset(statement);
        }
    }
}

- (NSMutableArray*) getAllBills
{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat: @"select * from bill"];
        const char *query_stmt = [querySQL UTF8String];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        
        if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                BillModel *bModel = [[BillModel alloc] init];
                bModel.billID = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
                
                bModel.billDesc = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 1)];
                
                bModel.amount = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 2)];
                
                bModel.dueDate = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 3)];
                
                bModel.isRepeat = [[[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 4)] boolValue];
                
                bModel.repeatUntil = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 5)];
                
                [resultArray addObject:bModel];
            }
            
            sqlite3_reset(statement);
            return resultArray;
        }
    }
    return nil;
}

#pragma mark - Cashflow Expense Methods

-(BOOL) saveCashFlowExpense:(CashFlowExpense*) cashFlowExpenseModel {
    
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"insert into cashflowexpense (cashflowID, expenseDesc,expenseAmount,isPositive,isCleared,headerID,isHeader,headerTitle ) values (\"%d\", \"%@\", \"%@\", \"%d\", \"%d\", \"%d\", \"%d\", \"%@\")",cashFlowExpenseModel.cashflowID, cashFlowExpenseModel.expenseDesc, cashFlowExpenseModel.expenseAmount, cashFlowExpenseModel.isPositive, cashFlowExpenseModel.isCleared, cashFlowExpenseModel.headerID,cashFlowExpenseModel.isHeader,cashFlowExpenseModel.headerTitle];
        
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            return YES;
        }
        else {
            return NO;
        }
        
        sqlite3_reset(statement);
        
    }
    return NO;
}

- (void) deleteCashflowExpense : (int) cashFlowExpenseID
{
    NSString *sql_str=[NSString stringWithFormat:@"DELETE FROM cashflowexpense where cashflowID = %d",cashFlowExpenseID];
    const char *sql = [sql_str UTF8String];
    
    sqlite3 *database;
    const char *dbpath = [databasePath UTF8String];
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        sqlite3_stmt *deleteStmt;
        if(sqlite3_prepare_v2(database, sql, -1, &deleteStmt, NULL) == SQLITE_OK)
        {
            
            if(sqlite3_step(deleteStmt) != SQLITE_DONE )
            {
                NSLog( @"Error: %s", sqlite3_errmsg(database) );
            }
            else
            {
                NSLog(@"No Error");
            }
        }
        sqlite3_finalize(deleteStmt);
    }
    sqlite3_close(database);
}

- (void) deleteAllCashflowExpense : (int) cashFlowID
{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat: @"DELETE from cashflowexpense where cashflowID=\"%d\"",cashFlowID];
        
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                
            }
            
            sqlite3_reset(statement);
        }
    }
}

- (NSMutableArray*) getAllCashflowExpense : (int) cashFlowID
{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat: @"select * from cashflowexpense where cashflowID=\"%d\"",cashFlowID];
        const char *query_stmt = [querySQL UTF8String];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        
        if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                CashFlowExpense *cModel = [[CashFlowExpense alloc] init];
                cModel.expenseID = [[[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)] intValue];
                cModel.cashflowID = [[[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 1)] intValue];
                
                cModel.expenseDesc = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 2)];
                
                cModel.expenseAmount = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 3)];
                
                cModel.isCleared = [[[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 4)] boolValue];
                
                cModel.isPositive = [[[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 5)] boolValue];
                
                cModel.headerID = [[[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 6)] intValue];
                cModel.isHeader = [[[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 7)] boolValue];
                cModel.headerTitle = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 8)];
                
                [resultArray addObject:cModel];
            }
            
            sqlite3_reset(statement);
            return resultArray;
        }
    }
    return nil;
}
//"create table if not exists cashflowexpense (expenseID integer primary key, cashflowID integer, expenseDesc text, expenseAmount text, isPositive integer, isCleared integer, headerID integer)"

-(BOOL) editCashFlowExpenseModel:(CashFlowExpense*) cfModel{
    
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        
        NSString *updateSQL = [NSString stringWithFormat:@"UPDATE cashflowexpense set cashflowID = '%d', expenseDesc = '%@', expenseAmount = '%@', isPositive = '%d' , isCleared = '%d', headerID = '%d', isHeader = '%d', headerTitle = '%@' WHERE expenseID = '%d'",
                               cfModel.cashflowID,
                               cfModel.expenseDesc,
                               cfModel.expenseAmount,
                               cfModel.isPositive,
                               cfModel.isCleared,
                               cfModel.headerID,
                               cfModel.isHeader,
                               cfModel.headerTitle,
                               cfModel.expenseID];
        
        const char *insert_stmt = [updateSQL UTF8String];
        
        sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            return YES;
        }
        else {
            return NO;
        }
        
        sqlite3_reset(statement);
        
    }
    return NO;
}

#pragma mark - Cashflow Methods

-(BOOL) saveCashFlow:(CashFlow*) cashFlowModel {
    
    
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"insert into cashflow (groupName, totalBalance) values (\"%@\", \"%@\")",cashFlowModel.groupName, cashFlowModel.totalBalance];
        
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            return YES;
        }
        else {
            return NO;
        }
        
        sqlite3_reset(statement);
        
    }
    return NO;
}
- (void) deleteCashflow : (int) cashflowID
{
    NSString *sql_str=[NSString stringWithFormat:@"DELETE FROM cashflow where cashflowID = %d",cashflowID];
    const char *sql = [sql_str UTF8String];
    
    sqlite3 *database;
    const char *dbpath = [databasePath UTF8String];
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        sqlite3_stmt *deleteStmt;
        if(sqlite3_prepare_v2(database, sql, -1, &deleteStmt, NULL) == SQLITE_OK)
        {
            
            if(sqlite3_step(deleteStmt) != SQLITE_DONE )
            {
                NSLog( @"Error: %s", sqlite3_errmsg(database) );
            }
            else
            {
                //  NSLog( @"row id = %d", (sqlite3_last_insert_rowid(database)+1));
                NSLog(@"No Error");
            }
        }
        sqlite3_finalize(deleteStmt);
    }
    sqlite3_close(database);
}
- (void) deleteAllCashflow
{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat: @"DELETE from cashflow"];
        const char *query_stmt = [querySQL UTF8String];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        
        if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                
            }
            
            sqlite3_reset(statement);
        }
    }
}
- (NSMutableArray*) getAllCashflowGroups
{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat: @"select * from cashflow"];
        const char *query_stmt = [querySQL UTF8String];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        
        if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                CashFlow *cModel = [[CashFlow alloc] init];
                cModel.cashFlowID = [[[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)] intValue];
                
                cModel.groupName = [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 1)];
                
                cModel.totalBalance = [[NSString alloc]initWithUTF8String:
                                      (const char *) sqlite3_column_text(statement, 2)];
                
                [resultArray addObject:cModel];
            }
            
            sqlite3_reset(statement);
            return resultArray;
        }
    }
    return nil;
}

#pragma mark - Account Methods

-(BOOL) saveAccountModel:(AccountModel*) accountModel {
    
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"insert into account (accountName, accountNumber, startingBalance, isPositive) values (\"%@\", \"%@\", \"%@\", \"%d\")",accountModel.accountName, accountModel.accountNumber, accountModel.startingBalance, accountModel.isPositive];
        
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            return YES;
        }
        else {
            return NO;
        }
        
        sqlite3_reset(statement);
        
    }
    return NO;
}
- (void) deleteAccount : (int) accountID
{
    NSString *sql_str=[NSString stringWithFormat:@"DELETE FROM account where accountID = %d",accountID];
    const char *sql = [sql_str UTF8String];
    
    sqlite3 *database;
    const char *dbpath = [databasePath UTF8String];
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        sqlite3_stmt *deleteStmt;
        if(sqlite3_prepare_v2(database, sql, -1, &deleteStmt, NULL) == SQLITE_OK)
        {
            
            if(sqlite3_step(deleteStmt) != SQLITE_DONE )
            {
                NSLog( @"Error: %s", sqlite3_errmsg(database) );
            }
            else
            {
                //  NSLog( @"row id = %d", (sqlite3_last_insert_rowid(database)+1));
                NSLog(@"No Error");
            }
        }
        sqlite3_finalize(deleteStmt);
    }
    sqlite3_close(database);
}
- (void) deleteAllAccounts
{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat: @"DELETE from account"];
        const char *query_stmt = [querySQL UTF8String];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        
        if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                
            }
            
            sqlite3_reset(statement);
        }
    }
}
- (NSMutableArray*) getAllAccounts 
{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat: @"select * from account"];
        const char *query_stmt = [querySQL UTF8String];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        
        if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                AccountModel *aModel = [[AccountModel alloc] init];
                aModel.accountID = [[[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)] intValue];
                aModel.accountNumber = [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 2)];
                aModel.accountName = [[NSString alloc]initWithUTF8String:
                                  (const char *) sqlite3_column_text(statement, 1)];
                aModel.startingBalance = [[NSString alloc]initWithUTF8String:
                                      (const char *) sqlite3_column_text(statement, 3)];
                aModel.isPositive = [[[NSString alloc]initWithUTF8String:
                                      (const char *) sqlite3_column_text(statement, 4)] boolValue];
                
                [resultArray addObject:aModel];
            }
            
            sqlite3_reset(statement);
            return resultArray;
        }
    }
    return nil;
}

#pragma mark - Account Entry Methods

-(BOOL) saveAccountEntryModel:(AccountEntry*) accountEntryModel andAccountID : (int) accountID {
    
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"insert into accountEntry (accountID, entryDesc, entryAmount, entryDate, isPositive, isCleared) values (\"%d\" ,\"%@\", \"%@\", \"%@\", \"%d\" , \"%d\")",accountID, accountEntryModel.entryDesc, accountEntryModel.entryAmount, accountEntryModel.entryDate, accountEntryModel.isPositive, accountEntryModel.isTranscationCleared];
        
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            return YES;
        }
        else {
            return NO;
        }
        
        sqlite3_reset(statement);
        
    }
    return NO;
}

-(BOOL) editAccountEntryModel:(AccountEntry*) accountEntryModel{
    
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        
        NSString *updateSQL = [NSString stringWithFormat:@"UPDATE accountEntry set accountID = '%d', entryDesc = '%@', entryAmount = '%@', entryDate = '%@', isPositive = '%d' , isCleared = '%d' WHERE accountEntryID = '%d'",
                               accountEntryModel.accountID,
                               accountEntryModel.entryDesc,
                               accountEntryModel.entryAmount,
                               accountEntryModel.entryDate,
                               accountEntryModel.isPositive,
                               accountEntryModel.isTranscationCleared,
                               accountEntryModel.accountEntryID];
        
        const char *insert_stmt = [updateSQL UTF8String];
        
        sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            return YES;
        }
        else {
            return NO;
        }
        
        sqlite3_reset(statement);
        
    }
    return NO;
}

- (NSMutableArray*) getAllAccountEntries : (int) accountID
{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat: @"select * from accountEntry where accountID=\"%d\"",accountID];
        const char *query_stmt = [querySQL UTF8String];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        
        if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                AccountEntry *aModel = [[AccountEntry alloc] init];
                aModel.accountEntryID = [[[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)] intValue];
                aModel.accountID = [[[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 1)] intValue];
                aModel.entryDesc = [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 2)];
                aModel.entryAmount = [[NSString alloc]initWithUTF8String:
                                      (const char *) sqlite3_column_text(statement, 3)];
                aModel.entryDate = [[NSString alloc]initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 4)];
                aModel.isPositive = [[[NSString alloc]initWithUTF8String:
                                      (const char *) sqlite3_column_text(statement, 5)] boolValue];
                aModel.isTranscationCleared = [[[NSString alloc]initWithUTF8String:
                                      (const char *) sqlite3_column_text(statement, 6)] boolValue];
                
                [resultArray addObject:aModel];
            }
            
            sqlite3_reset(statement);
            return resultArray;
        }
    }
    return nil;
}

- (void) deleteAccountEntry : (int) accountEntryID
{
    NSString *sql_str=[NSString stringWithFormat:@"DELETE FROM accountEntry where accountEntryID = %d",accountEntryID];
    const char *sql = [sql_str UTF8String];
    
    sqlite3 *database;
    const char *dbpath = [databasePath UTF8String];
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        sqlite3_stmt *deleteStmt;
        if(sqlite3_prepare_v2(database, sql, -1, &deleteStmt, NULL) == SQLITE_OK)
        {
            
            if(sqlite3_step(deleteStmt) != SQLITE_DONE )
            {
                NSLog( @"Error: %s", sqlite3_errmsg(database) );
            }
            else
            {
                //  NSLog( @"row id = %d", (sqlite3_last_insert_rowid(database)+1));
                NSLog(@"No Error");
            }
        }
        sqlite3_finalize(deleteStmt);
    }
    sqlite3_close(database);
}

- (void) deleteAllAccountEntries : (int) accountID
{
    NSString *sql_str=[NSString stringWithFormat:@"DELETE FROM accountEntry where accountID = %d",accountID];
    const char *sql = [sql_str UTF8String];
    
    sqlite3 *database;
    const char *dbpath = [databasePath UTF8String];
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        sqlite3_stmt *deleteStmt;
        if(sqlite3_prepare_v2(database, sql, -1, &deleteStmt, NULL) == SQLITE_OK)
        {
            
            if(sqlite3_step(deleteStmt) != SQLITE_DONE )
            {
                NSLog( @"Error: %s", sqlite3_errmsg(database) );
            }
            else
            {
                //  NSLog( @"row id = %d", (sqlite3_last_insert_rowid(database)+1));
                NSLog(@"No Error");
            }
        }
        sqlite3_finalize(deleteStmt);
    }
    sqlite3_close(database);
}
@end

