//
//  ViewController.h
//  BankR
//
//  Created by Ahmed Sadiq on 26/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>

@interface ViewController : UIViewController <UITableViewDelegate,UITableViewDataSource> {
    float grandTotal;
}

@property (strong, nonatomic) IBOutlet UITableView *bankTbView;
@property (strong,nonatomic) AVPlayer *avPlayer;
@property (strong, nonatomic) IBOutlet UIButton *doneBtn;
@property (strong, nonatomic) IBOutlet UIButton *optionsBtn;
@property (strong, nonatomic) NSMutableArray *accountsArray;
@property (strong, nonatomic) IBOutlet UILabel *totalAmount;
@property (strong, nonatomic) IBOutlet UILabel *noAccountLbl;

- (IBAction)doneBtnPressed:(id)sender;

- (IBAction)optionsBtnPressed:(id)sender;
@end

