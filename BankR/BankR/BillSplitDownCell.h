//
//  BillSplitDownCell.h
//  BankR
//
//  Created by Ahmed Sadiq on 02/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BillSplitDownCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *amountTxt;
@property (strong, nonatomic) IBOutlet UILabel *paidDateTxt;
@end
