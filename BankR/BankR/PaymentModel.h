//
//  PaymentModel.h
//  BankR
//
//  Created by Ahmed Sadiq on 18/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PaymentModel : NSObject
@property (strong, nonatomic) NSString *paymentID;
@property (strong, nonatomic) NSString *billID;
@property (strong, nonatomic) NSString *paymentDate;
@property (strong, nonatomic) NSString *amount;
@end
