//
//  CashFlowVC.m
//  BankR
//
//  Created by Ahmed Sadiq on 26/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "DBManager.h"
#import "CashFlowVC.h"
#import "CashflowCell.h"
#import "CashFlow.h"
#import "Utils.h"
#import "CashFlowGroupDetailsVC.h"

@interface CashFlowVC ()

@end

@implementation CashFlowVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    cashflowArray = [[DBManager getSharedInstance] getAllCashflowGroups];
    [_cashflowTblView reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return cashflowArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"CashflowCell";
    CashflowCell *cell = (CashflowCell *)[_cashflowTblView dequeueReusableCellWithIdentifier:nil];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CashflowCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    CashFlow *cModel = [cashflowArray objectAtIndex:indexPath.row];
    cell.amountLbl.text = [self getTotalBalanceForCashFlow:cModel.cashFlowID];
    cell.titleLbl.text = cModel.groupName;
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (NSString *) getTotalBalanceForCashFlow : (int) cashFlowID {
    NSMutableArray *transcationsArray = [[DBManager getSharedInstance] getAllCashflowExpense:cashFlowID];
    
    int totalAmountInt = 0;
    
    for(int i=0; i<transcationsArray.count; i++) {
        CashFlowExpense *cfModel = [transcationsArray objectAtIndex:i];
        
        if(cfModel.isPositive) {
            totalAmountInt = totalAmountInt + [cfModel.expenseAmount intValue];
        }
        else {
            totalAmountInt = totalAmountInt - [cfModel.expenseAmount intValue];
        }
    }
    return [NSString stringWithFormat:@"$%d",totalAmountInt];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 58;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(isDuplicate) {
        
        CashFlow *cExModel = [cashflowArray objectAtIndex:indexPath.row];
        
        cExModel.groupName = [NSString stringWithFormat:@"%@ (Duplicate)",cExModel.groupName];
        [[DBManager getSharedInstance] saveCashFlow:cExModel];
        
        NSMutableArray *transcationsArray = [[DBManager getSharedInstance] getAllCashflowExpense:cExModel.cashFlowID];
        NSMutableArray *transcationsArrayTemp = [[DBManager getSharedInstance] getAllCashflowGroups];
        CashFlow *tempModel = [transcationsArrayTemp objectAtIndex:transcationsArrayTemp.count-1];
        
        for(int i=0; i<transcationsArray.count; i++) {
            CashFlowExpense *cfModel = [transcationsArray objectAtIndex:i];
            cfModel.cashflowID = tempModel.cashFlowID;
            [[DBManager getSharedInstance] saveCashFlowExpense:cfModel];
            
        }
        
        isDuplicate = false;
        
        cashflowArray = [[DBManager getSharedInstance] getAllCashflowGroups];
        [tableView reloadData];
        
        
        
    }
    else {
        CashFlow *cExModel = [cashflowArray objectAtIndex:indexPath.row];
        CashFlowGroupDetailsVC *cashFlowGroupDetailsVC = [[CashFlowGroupDetailsVC alloc] initWithNibName:@"CashFlowGroupDetailsVC" bundle:nil];
        cashFlowGroupDetailsVC.cModel = cExModel;
        [self.navigationController pushViewController:cashFlowGroupDetailsVC animated:YES];
        [self.navigationController setNavigationBarHidden:YES];
    }
    
    
}

- (UITableViewCellEditingStyle) tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleDelete;
}

- (BOOL) tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void) tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    
    CashFlow *aModelTemp = [cashflowArray objectAtIndex:sourceIndexPath.row];
    [cashflowArray removeObjectAtIndex:sourceIndexPath.row];
    [cashflowArray insertObject:aModelTemp atIndex:destinationIndexPath.row];
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //remove the deleted object from your data source.
        //If your data source is an NSMutableArray, do this
        
        CashFlow *cModel = [cashflowArray objectAtIndex:indexPath.row];
        [[DBManager getSharedInstance] deleteCashflow:cModel.cashFlowID];
        [cashflowArray removeObject:cModel];
        [tableView reloadData];
    }
}

- (IBAction)optionsPressed:(id)sender {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    actionSheet.view.tintColor = [UIColor colorWithRed:0.549 green:0.549 blue:0.549 alpha:1.0];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped do nothing.
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Add New Group" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"New Group"
                                                                       message:@"Enter a name for this group."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            // optionally configure the text field
            textField.keyboardType = UIKeyboardTypeAlphabet;
        }];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Save"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action) {
                                                             
                                                             UITextField *textField = [alert.textFields firstObject];
                                                             
                                                             if(textField.text.length > 0) {
                                                                 CashFlow *cModel = [[CashFlow alloc] init];
                                                                 cModel.groupName = textField.text;
                                                                 cModel.totalBalance = @"0";
                                                                 [[DBManager getSharedInstance] saveCashFlow:cModel];
                                                                 
                                                                 cashflowArray = [[DBManager getSharedInstance] getAllCashflowGroups];
                                                                 [_cashflowTblView reloadData];
                                                             }
                                                             else {
                                                                 [self presentViewController:[Utils showCustomAlert:@"Validation Error" andMessage:@"Group name not in proper format"] animated:YES completion:nil];
                                                             }
                                                             
                                                         }];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action) {
                                                             
                                                         }];
        
        [alert addAction:cancelAction];
        [alert addAction:okAction];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Duplicate Group" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        isDuplicate = true;
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Rearrange" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        _optionsBtn.hidden = TRUE;
        _doneBtn.hidden = FALSE;
        [_cashflowTblView setEditing:TRUE animated:YES];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Delete Group" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        _optionsBtn.hidden = TRUE;
        _doneBtn.hidden = FALSE;
        
        [_cashflowTblView setEditing:TRUE animated:YES];
        
    }]];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (IBAction)donePressed:(id)sender {
    
    [_cashflowTblView setEditing:false animated:YES];
    
    _optionsBtn.hidden = false;
    _doneBtn.hidden = true;
    
    [[DBManager getSharedInstance] deleteAllCashflow];
    
    for(int i=0; i<cashflowArray.count; i++) {
        CashFlow *cModel = [cashflowArray objectAtIndex:i];
        [[DBManager getSharedInstance] saveCashFlow:cModel];
    }
}
@end
