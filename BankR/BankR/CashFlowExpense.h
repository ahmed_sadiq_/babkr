//
//  CashFlowExpense.h
//  BankR
//
//  Created by Ahmed Sadiq on 14/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CashFlowExpense : NSObject

@property int headerID;
@property int expenseID;
@property int cashflowID;
@property BOOL isPositive;
@property BOOL isCleared;
@property (strong, nonatomic) NSString *expenseDesc;
@property (strong, nonatomic) NSString *expenseAmount;
@property BOOL isHeader;
@property (strong, nonatomic) NSString *headerTitle;

@end
