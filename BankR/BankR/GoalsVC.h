//
//  GoalsVC.h
//  BankR
//
//  Created by Ahmed Sadiq on 26/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoalsVC : UIViewController <UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *goalsTblview;
@property (strong, nonatomic) IBOutlet UIButton *doneBtn;
@property (strong, nonatomic) IBOutlet UIButton *optionsBtn;
@property (strong, nonatomic) NSMutableArray *goalsArray;
@property (strong, nonatomic) NSMutableArray *inProgressArray;
@property (strong, nonatomic) NSMutableArray *completedArray;

- (IBAction)doneBtnPressed:(id)sender;
@end
